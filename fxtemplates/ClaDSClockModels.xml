
<beast version='2.0'
       namespace='beastfx.app.beauti:beast.pkgmgmt:beast.base.core:beast.base.inference:beast.base.evolution.branchratemodel:beast.base.evolution.speciation:beast.base.evolution.tree.coalescent:beast.base.util:beast.base.math:beast.evolution.nuc:beast.base.evolution.operator:beast.base.inference.operator:beast.base.evolution.sitemodel:beast.base.evolution.substitutionmodel:beast.base.evolution.likelihood:beast.evolution:beast.base.inference.distribution'>


    <!-- Clock models -->
    <mergewith point='ClaDSclockModelTemplates'>

        <!-- Strict clock -->
        <subtemplate id='StrictClock' class='beast.base.evolution.branchratemodel.StrictClockModel'
                     mainid='StrictClock.c:$(n)'>
            
            <![CDATA[
            	<branchRateModel spec='StrictClockModel' id='StrictClock.c:$(n)'>
                    <clock.rate id='clockRate.c:$(n)' spec='parameter.RealParameter' value='1.0' lower="0.0" estimate='false'/>
                </branchRateModel>
                
                <operator id="StrictClockRateScaler.c:$(n)" spec="beast.base.evolution.operator.AdaptableOperatorSampler" weight="1.5">
	                <parameter idref="clockRate.c:$(n)"/>
	    	        <operator idref="AVMNOperator.$(n)"/>
	        	    <operator id='StrictClockRateScalerX.c:$(n)' spec='kernel.BactrianScaleOperator' scaleFactor="0.75" weight="3" parameter='@clockRate.c:$(n)'/>
	        	</operator>

				<prior id='ClockPrior.c:$(n)' x='@clockRate.c:$(n)'>
					<Gamma name="distr" alpha="0.01" beta="100.0"/>
				</prior>
			]]>
			
            <connect srcID='clockRate.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(clockRate.c:$(n)) and clockRate.c:$(n)/estimate=true'/>
            <connect srcID='clockRate.c:$(n)' targetID='AVMNLogTransform.$(n)' inputName='f' if='inlikelihood(clockRate.c:$(n)) and clockRate.c:$(n)/estimate=true'/>
            <connect srcID='ClockPrior.c:$(n)' targetID='prior' inputName='distribution' if='inlikelihood(clockRate.c:$(n)) and clockRate.c:$(n)/estimate=true'>substitution rate of partition c:$(n)</connect>
            <connect srcID='StrictClockRateScaler.c:$(n)' targetID='mcmc' inputName='operator' if='inlikelihood(clockRate.c:$(n)) and clockRate.c:$(n)/estimate=true'>Scale substitution rate of partition c:$(n)</connect>
            <connect srcID='clockRate.c:$(n)' targetID='augTUpDown.t:$(n)' inputName='parameterInverse' if='inlikelihood(Tree.t:$(n)) and Tree.t:$(n)/estimateTopology=true 
            	and inlikelihood(clockRate.c:$(n)) and clockRate.c:$(n)/estimate=true'/>
        </subtemplate>


        <!-- Random local clock -->
        <subtemplate id='RandomLocalClock' class='beast.base.evolution.branchratemodel.RandomLocalClockModel'
                     mainid='RandomLocalClock.c:$(n)'
                     suppressInputs='beast.base.evolution.branchratemodel.RandomLocalClockModel.rates'
                     hmc='
					 RandomLocalClock/clock.rate/=StrictClock/clock.rate/,
					 RandomLocalClock/scaling/,
					 RandomLocalClock/includeRoot/,
					 RandomLocalClock/ratesAreMultipliers/,
                     RRatesPrior/index/=RandomLocalClock/RatesPrior/,
                     RRateChangesPrior/index/=RandomLocalClock/RRateChangesPrior/,
                     MeanRRatePrior/index/=Priors/ClockPrior/,
                     ClockRateScaler/index/=Operators/BactrianRateScaler/index/,
                     randomClockScaler/index/=Operators/BactrianRateScaler/index/,
                     randomClockUpDownOperator/index/=Operators/BactrianUpDown/index/,
                     BitFlipOperator/index/=Operators/BitFlipper/index/'>
                     
            <![CDATA[
		        <input spec='RandomLocalClockModel' id="RandomLocalClock.c:$(n)" ratesAreMultipliers="false" tree='@Tree.t:$(n)'>
		            <clock.rate id='meanClockRate.c:$(n)' spec='parameter.RealParameter' value='1.0' lower="0.0" estimate='false'/>
		            <parameter spec='parameter.BooleanParameter' name='indicators' id='Indicators.c:$(n)' value="0"/>
		            <parameter name='rates' id='clockrates.c:$(n)' value="1" lower="1e-9"/>
		        </input>

		        <operator id="IndicatorsBitFlip.c:$(n)" spec="BitFlipOperator" weight="15" parameter="@Indicators.c:$(n)"/>
				<!-- this should probably be a DeltaExchange instead of scale operator to keep mean rate to 1 -->
		        <operator id='ClockRateScaler.c:$(n)' spec='kernel.BactrianScaleOperator' scaleAll='false' scaleFactor="0.1" weight="15" parameter="@clockrates.c:$(n)"/>		
		        <operator id='randomClockScaler.c:$(n)' spec='kernel.BactrianScaleOperator' scaleAll='false' scaleFactor="0.1" weight="1" parameter="@meanClockRate.c:$(n)"/>

				<distribution idref="prior">
					<if cond='inlikelihood(RandomLocalClock.c:$(n))'>
						<!-- prior on rates -->
					    <prior id='RRatesPrior.c:s$(n)' name="distribution" x='@clockrates.c:$(n)'>
					        <Gamma id="Gamma.0" name="distr">
					            <parameter estimate="false" name="alpha">0.5</parameter>
					            <parameter estimate="false" name="beta">2.0</parameter>
					        </Gamma>
						</prior>
		
						<!-- prior on number of changes -->
						<prior id="RRateChangesPrior.c:$(n)" name="distribution">
							<x id="RRateChanges.c:$(n)" spec="beast.base.evolution.Sum" arg="@Indicators.c:$(n)"/>
		                    <distr spec='beast.base.inference.distribution.Poisson'>
		                          <lambda spec='parameter.RealParameter' estimate='false' value='0.6931471805599453'/>
		                    </distr>
						</prior>
					</if>
				</distribution>
		
				<logger idref="tracelog">
					<if cond='inlikelihood(RandomLocalClock.c:$(n))'>
						<log idref="RRateChanges.c:$(n)"/>
					</if>
				</logger>
		
		      <prior id='MeanRRatePrior.c:s$(n)' x='@meanClockRate.c:$(n)'>
                  <Gamma  name="distr">
                  	<alpha spec="parameter.RealParameter" value="0.01" estimate="false"/> 
                  	<beta  spec="parameter.RealParameter" value="100.0" estimate="false"/>
                  </Gamma>
		      </prior>
			]]>
			
            <connect srcID='Indicators.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(Indicators.c:$(n))'/>
            <connect srcID='meanClockRate.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(meanClockRate.c:$(n)) and meanClockRate.c:$(n)/estimate=true'/>
            <connect srcID='meanClockRate.c:$(n)' targetID='AVMNLogTransform.$(n)' inputName='f' if='inlikelihood(meanClockRate.c:$(n)) and meanClockRate.c:$(n)/estimate=true'/>
            <connect srcID='clockrates.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(clockrates.c:$(n))'/>

            <connect srcID='randomClockScaler.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(meanClockRate.c:$(n)) and meanClockRate.c:$(n)/estimate=true'>Scale clock rate of partition c:$(n)</connect>            
            <connect srcID='meanClockRate.c:$(n)' targetID='augTUpDown.t:$(n)' inputName='parameterInverse'
                     if='inlikelihood(meanClockRate.c:$(n)) and inlikelihood(Tree.t:$(n)) and Tree.t:$(n)/estimate=true and meanClockRate.c:$(n)/estimate=true'>Up/down scaler for mean rate and tree of partition c:$(n)</connect>                
            <connect srcID='IndicatorsBitFlip.c:$(n)' targetID='mcmc' inputName='operator' if='inlikelihood(Indicators.c:$(n))'>Flip indicator bits for random clock of partition c:$(n)</connect>
            <connect srcID='ClockRateScaler.c:$(n)' targetID='mcmc' inputName='operator' if='inlikelihood(clockrates.c:$(n))'>Scale random clock rates of partition c:$(n)</connect>
            <connect srcID='Tree.t:$(n)' targetID='RandomLocalClock.c:$(n)' inputName='tree' if='inlikelihood(RandomLocalClock.c:$(n))'/>

            <connect srcID='Indicators.c:$(n)' targetID='tracelog' inputName='log' if='inlikelihood(Indicators.c:$(n))'/>
            <connect srcID='clockrates.c:$(n)' targetID='tracelog' inputName='log' if='inlikelihood(clockrates.c:$(n))'/>
            <connect srcID='meanClockRate.c:$(n)' targetID='tracelog' inputName='log' if='inposterior(RandomLocalClock.c:$(n)) and meanClockRate.c:$(n)/estimate=true'/>

            <connect srcID='MeanRRatePrior.c:$(n)' targetID='prior' inputName='distribution' if='inlikelihood(meanClockRate.c:$(n)) and meanClockRate.c:$(n)/estimate=true'>substitution rate of partition c:$(n)</connect>

            <connect srcID='RandomLocalClock.c:$(n)' targetID='TreeWithMetaDataLogger.t:$(n)' inputName='branchratemodel' if='inposterior(RandomLocalClock.c:$(n))'/>
        </subtemplate>
        
        <subtemplate id='RelaxedClockLogNormal' class='beast.base.evolution.branchratemodel.UCRelaxedClockModel'
                     mainid='RelaxedClock.c:$(n)'
                     suppressInputs='beast.base.evolution.branchratemodel.UCRelaxedClockModel.distr,beast.base.evolution.branchratemodel.UCRelaxedClockModel.rateQuantiles,beast.base.evolution.branchratemodel.UCRelaxedClockModel.rates'>
                     
            <![CDATA[
		        <plugin spec='UCRelaxedClockModel' id="RelaxedClock.c:$(n)" tree='@Tree.t:$(n)'>
					<parameter name='clock.rate' id='ucldMean.c:$(n)' value='1.0'/>
		            <distr id='LogNormalDistributionModel.c:$(n)' name='distr' spec="beast.base.inference.distribution.LogNormalDistributionModel" meanInRealSpace='true'>
		                <parameter name='M' value="1.0" estimate='false' lower='0' upper='1'/>
		                <parameter name='S' id='ucldStdev.c:$(n)' value="0.1" lower="0" estimate='true'/>
		            </distr>
		            <rateCategories spec='parameter.IntegerParameter' id='rateCategories.c:$(n)' value="1" dimension='10' estimate='true'/>
		        </plugin>
		
		        <operator id='ucldMeanScaler.c:$(n)' spec='kernel.BactrianScaleOperator' scaleAll='false' scaleFactor="0.5" weight="1" parameter="@ucldMean.c:$(n)"/>
		        <operator id='ucldStdevScaler.c:$(n)' spec='kernel.BactrianScaleOperator' scaleAll='false' scaleFactor="0.5" weight="3" parameter="@ucldStdev.c:$(n)"/>
		        <operator id="CategoriesRandomWalk.c:$(n)" spec="IntRandomWalkOperator" windowSize='1' weight="10" parameter="@rateCategories.c:$(n)"/>
				<operator id='CategoriesSwapOperator.c:$(n)' spec='SwapOperator' howMany="1" weight="10" intparameter='@rateCategories.c:$(n)'/>
				<operator id='CategoriesUniform.c:$(n)' spec='UniformOperator' weight="10" parameter='@rateCategories.c:$(n)'/>
		
		        <prior id='ucldStdevPrior.c:$(n)' x='@ucldStdev.c:$(n)'>		
		            <!-- this prior has a median of 0.1 and 97.5% of its probability is below S=1 -->
		            <distr spec="beast.base.inference.distribution.Gamma" id="Gamma.std">
		                <parameter name='alpha' value="0.5396" estimate='false'/> <!-- shape -->
		                <parameter name='beta' value="0.3819" estimate='false'/> <!-- scale -->
		            </distr>
		        </prior>
		        <prior id='MeanRatePrior.c:$(n)' x='@ucldMean.c:$(n)'>
					<Gamma name="distr" alpha="0.01" beta="100.0"/>
				</prior>		
		
				<log id='rate.c:$(n)' spec='beast.base.evolution.RateStatistic' tree='@Tree.t:$(n)' branchratemodel='@RelaxedClock.c:$(n)'/>
			]]>
			
            <connect srcID='ucldMean.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(ucldMean.c:$(n)) and ucldMean.c:$(n)/estimate=true'/>
            <connect srcID='ucldStdev.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(ucldStdev.c:$(n)) and ucldStdev.c:$(n)/estimate=true'/>
            <connect srcID='rateCategories.c:$(n)' targetID='state' inputName='stateNode' if='inlikelihood(rateCategories.c:$(n))'/>
            <connect srcID='ucldMean.c:$(n)' targetID='AVMNLogTransform.$(n)' inputName='f' if='inlikelihood(ucldMean.c:$(n)) and ucldMean.c:$(n)/estimate=true'/>

            <connect srcID='ucldMeanScaler.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(ucldMean.c:$(n)) and ucldMean.c:$(n)/estimate=true'>Scale clock rate of partition c:$(n)</connect>
            <connect srcID='ucldStdevScaler.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(ucldStdev.c:$(n)) and ucldStdev.c:$(n)/estimate=true'>Scale stdev of rate of partition c:$(n)</connect>
            <connect srcID='CategoriesRandomWalk.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(rateCategories.c:$(n)) and rateCategories.c:$(n)/estimate=true'>Randomly change categories of partition c:$(n)</connect>
            <connect srcID='CategoriesSwapOperator.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(rateCategories.c:$(n)) and rateCategories.c:$(n)/estimate=true'>Swap categories of partition c:$(n)</connect>
            <connect srcID='CategoriesUniform.c:$(n)' targetID='mcmc' inputName='operator'
                     if='inlikelihood(rateCategories.c:$(n)) and rateCategories.c:$(n)/estimate=true'>Uniformly draw categories of partition c:$(n)</connect>
            <connect srcID='ucldMean.c:$(n)' targetID='augTUpDown.t:$(n)' inputName='parameterInverse'
                     if='inlikelihood(ucldMean.c:$(n)) and inlikelihood(Tree.t:$(n)) and Tree.t:$(n)/estimate=true and ucldMean.c:$(n)/estimate=true'>Up/down scaler for mean rate and tree of partition c:$(n)</connect>                
            
            <connect srcID='Tree.t:$(n)' targetID='RelaxedClock.c:$(n)' inputName='tree' if='inlikelihood(RelaxedClock.c:$(n))'/>
            <connect srcID='Tree.t:$(n)' targetID='rate.c:$(n)' inputName='tree' if='inlikelihood(RelaxedClock.c:$(n))'/>

            <connect srcID='ucldMean.c:$(n)' targetID='tracelog' inputName='log' if='inlikelihood(ucldMean.c:$(n)) and ucldMean.c:$(n)/estimate=true'/>
            <connect srcID='ucldStdev.c:$(n)' targetID='tracelog' inputName='log' if='inlikelihood(ucldStdev.c:$(n))'/>
            <connect srcID='rate.c:$(n)' targetID='tracelog' inputName='log' if='inposterior(RelaxedClock.c:$(n))'/>

            <connect srcID='ucldStdevPrior.c:$(n)' targetID='prior' inputName='distribution' if='inlikelihood(ucldStdev.c:$(n))'>uncorrelated lognormal relaxed clock stdev of partition c:$(n)</connect>
            <connect srcID='MeanRatePrior.c:$(n)' targetID='prior' inputName='distribution'
                     if='inlikelihood(ucldMean.c:$(n)) and inlikelihood(Tree.t:$(n)) and ucldMean.c:$(n)/estimate=true'>uncorrelated lognormal relaxed clock mean of partition c:$(n)</connect>

            <connect srcID='RelaxedClock.c:$(n)' targetID='TreeWithMetaDataLogger.t:$(n)' inputName='branchratemodel' if='inposterior(RelaxedClock.c:$(n))'/>
        </subtemplate>

    </mergewith>

</beast>




