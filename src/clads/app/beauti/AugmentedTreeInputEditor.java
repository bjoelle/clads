/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.app.beauti;

import beast.base.core.BEASTInterface;
import beast.base.core.Input;
import beast.base.core.Log;
import beast.base.inference.parameter.RealParameter;
import beastfx.app.inputeditor.BeautiDoc;
import beastfx.app.inputeditor.InputEditor;
import beastfx.app.inputeditor.ParameterInputEditor;
import beastfx.app.util.FXUtils;
import clads.evolution.tree.AugmentedTree;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;

/**
 * Shows and allows editing of an augmented tree in Beauti.
 */
public class AugmentedTreeInputEditor extends InputEditor.Base {

	AugmentedTree augTree;

	CheckBox useTurnoverCheckBox;
	InputEditor.Base lambda0InputEditor, alphaInputEditor, sigmaInputEditor, mu0InputEditor, alphaMInputEditor, sigmaMInputEditor,
			turnoverInputEditor, rhoInputEditor, psiInputEditor;

	/**
	 * Instantiates a new augmented tree input editor.
	 *
	 * @param doc the beauti doc
	 */
	public AugmentedTreeInputEditor(BeautiDoc doc) {
		super(doc);

		lambda0InputEditor = new ParameterInputEditor(doc);
		alphaInputEditor = new ParameterInputEditor(doc);
		sigmaInputEditor = new ParameterInputEditor(doc);

		mu0InputEditor = new ParameterInputEditor(doc);
		alphaMInputEditor = new ParameterInputEditor(doc);
		sigmaMInputEditor = new ParameterInputEditor(doc);
		turnoverInputEditor = new ParameterInputEditor(doc);

		rhoInputEditor = new ParameterInputEditor(doc);
		psiInputEditor = new ParameterInputEditor(doc);
	}

	/**
	 * @return the class of object to edit
	 */
	@Override
	public Class<?> type() {
		return AugmentedTree.class;
	}

	@Override
	public void init(Input<?> input, BEASTInterface plugin, int itemNr, ExpandOption bExpandOption, boolean bAddButtons) {
		// Set up fields
		m_bAddButtons = bAddButtons;
		m_input = input;
		m_beastObject = plugin;
		this.itemNr = itemNr;

		pane = FXUtils.newVBox();

		// Adds label to left of input editor
		addInputLabel();

		// Create component models and fill them with data from input
		augTree = (AugmentedTree) input.get();

		useTurnoverCheckBox = new CheckBox("Use fixed turnover (=death/birth rate)");
		useTurnoverCheckBox.setId(input.getName() + ".isEstimated");
		useTurnoverCheckBox.setSelected(augTree.useTurnoverInput.get());
		useTurnoverCheckBox.setTooltip(new Tooltip(augTree.useTurnoverInput.getTipText()));

		loadFromAugmentedTree();

		addLambdaFields(bExpandOption, bAddButtons);

		addMuFields(bExpandOption, bAddButtons);

		addEndFields(bExpandOption, bAddButtons);

		getChildren().add(pane);

		addEventListeners();
		addValidationLabel();
	}

	/**
	 * Load data from the augmented tree.
	 */
	public void loadFromAugmentedTree() {
		useTurnoverCheckBox.setSelected(augTree.isTurnoverUsed());
	}

	/**
	 * Save data to the augmented tree.
	 */
	public void saveToAugmentedTree() {
		augTree.useTurnoverInput.setValue(useTurnoverCheckBox.isSelected(), augTree);

		validateInput();
		refreshPanel();
	}

	/**
	 * Adds the lambda related fields.
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	protected void addLambdaFields(ExpandOption bExpandOption, boolean bAddButtons) {
		this.addInput(lambda0InputEditor, augTree.lambda0Input, bExpandOption, bAddButtons);
		this.addInput(alphaInputEditor, augTree.alphaInput, bExpandOption, bAddButtons);
		this.addInput(sigmaInputEditor, augTree.sigmaInput, bExpandOption, bAddButtons);
	}

	/**
	 * Adds the mu fields depending on parametrization.
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	protected void addMuFields(ExpandOption bExpandOption, boolean bAddButtons) {
		pane.getChildren().add(useTurnoverCheckBox);

		if (augTree.isTurnoverUsed()) {
			this.addInput(turnoverInputEditor, augTree.turnoverInput, bExpandOption, bAddButtons);
		} else {
			this.addInput(mu0InputEditor, augTree.mu0Input, bExpandOption, bAddButtons);
			this.addInput(alphaMInputEditor, augTree.alphaMInput, bExpandOption, bAddButtons);
			this.addInput(sigmaMInputEditor, augTree.sigmaMInput, bExpandOption, bAddButtons);
		}
	}

	/**
	 * Adds the end fields (sampling probs).
	 *
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	protected void addEndFields(ExpandOption bExpandOption, boolean bAddButtons) {
		this.addInput(psiInputEditor, augTree.psiInput, bExpandOption, bAddButtons);
		this.addInput(rhoInputEditor, augTree.rhoInput, bExpandOption, bAddButtons);
	}

	/**
	 * Adds an input editor for a real parameter.
	 *
	 * @param inputEditor the input editor
	 * @param input the corresponding input
	 * @param bExpandOption the expand option
	 * @param bAddButtons the add buttons option
	 */
	void addInput(Base inputEditor, Input<RealParameter> input, ExpandOption bExpandOption, boolean bAddButtons) {
		inputEditor.init(input, augTree, itemNr, bExpandOption, bAddButtons);
		inputEditor.addValidationListener(this);
		pane.getChildren().add(inputEditor);
	}

	/**
	 * Adds the event listeners.
	 */
	protected void addEventListeners() {
		useTurnoverCheckBox.setOnAction(e -> {
			augTree.useTurnoverInput.setValue(useTurnoverCheckBox.isSelected(), augTree);
			augTree.initAndValidate();
			sync();
			refreshPanel();
		});
	}

	/**
	 * Validate the inputs.
	 */
	@Override
	public void validateInput() {
		try {
			augTree.initAndValidate();
			if (m_validateLabel != null) {
				m_validateLabel.setVisible(false);
			}
		} catch (Exception e) {
			Log.err.println("Validation message: " + e.getMessage());
			if (m_validateLabel != null) {
				m_validateLabel.setTooltip("Validation error: " + e.getMessage());
				m_validateLabel.setColor("red");
				m_validateLabel.setVisible(true);
			}
			notifyValidationListeners(ValidationStatus.IS_INVALID);
		}
		repaint();
	}

}
