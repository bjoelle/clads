/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.evolution.speciation;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.ContinuousDistribution;
import org.apache.commons.math.distribution.Distribution;

import beast.base.core.Input;
import beast.base.inference.distribution.ParametricDistribution;

/**
 * Prior distribution for a fixed parameter (used for testing).
 */
public class PointDistribution extends ParametricDistribution {

	public Input<Double> pointInput = new Input<>("point", "point of the distribution, default 0", 0.0);
	public Input<Double> precisionInput = new Input<>("precision", "accuracy of the point, default 1e-5", 1e-5);

	PointImpl distr = new PointImpl();
	double point, precision;

	@Override
	public void initAndValidate() {
		point = pointInput.get();
		precision = precisionInput.get();
		distr.setPoint(point);
		distr.setPrecision(precision);
	}

	/**
	 * Implementation of the PointDistribution.
	 */
	class PointImpl implements ContinuousDistribution {
		private double pt, prec;

		/**
		 * Sets the point.
		 *
		 * @param point the new point
		 */
		public void setPoint(final double point) {
			this.pt = point;
		}

		/**
		 * Sets the precision.
		 *
		 * @param precision the new precision
		 */
		public void setPrecision(double precision) {
			prec = precision;
		}

		@Override
		public double cumulativeProbability(double x) throws MathException {
			if (x > pt - prec) return 1.0;
			return 0.0;
		}

		@Override
		public double cumulativeProbability(double x0, double x1) {
			if (x0 < pt && x1 > pt) {
				return 1.0;
			}
			return 0.0;
		}

		@Override
		public double inverseCumulativeProbability(final double p) throws MathException {
			if (p < 0.0 || p > 1.0) {
				throw new RuntimeException("inverseCumulativeProbability::argument out of range [0...1]");
			}
			if (p == 0) {
				// works even when one bound is infinite
				return pt - 1;
			}
			return pt;
		}

		@Override
		public double density(final double x) {
			if (Math.abs(x - pt) < prec) return 1.0;
			return 0.0;
		}

		@Override
		public double logDensity(final double x) {
			return Math.log(density(x));
		}
	} // class UniformImpl

	@Override
	public Distribution getDistribution() {
		return distr;
	}

	@Override
	public double density(final double x) {
		return distr.density(x);
	}

	@Override
	public double getMean() {
		return offsetInput.get() + point;
	}
}
