/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.evolution.speciation;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.TreeDistribution;
import clads.evolution.tree.AugmentedNode;
import clads.evolution.tree.AugmentedTree;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

/**
 * Tree likelihood class for ClaDS model on an augmented tree.
 */
public class ClaDSLikelihood extends TreeDistribution {

	public Input<AugmentedTree> augTreeInput = new Input<>("augmentedTree", "Tree with data augmentation", Validate.REQUIRED);
	public Input<Boolean> conditionOnSurvival = new Input<>("conditionOnSurvival", "Whether to condition the likelihood on tree survival, default false.", false);

	AugmentedTree augTree;

	@Override
	public void initAndValidate() {
		augTree = augTreeInput.get();

		super.initAndValidate();
	}

	/**
	 * Calculate the log likelihood of the full tree.
	 *
	 * @return the log likelihood
	 */
	@Override
	public double calculateLogP() {
		logP = calculateLogPHelper(true);
		return logP;
	}

	/**
	 * Calculate the full or non-stochastic log likelihood of the full tree.
	 * (both are identical if not conditioning on survival)
	 *
	 * @param stochastic whether to add the stochastic part
	 * @return the log likelihood
	 */
	private double calculateLogPHelper(boolean stochastic) {
		logP = 0.0;
		augTree.updateRates();

		if(stochastic && conditionOnSurvival.get()) logP -= getSurvivalLogProbability();

		for (Node n : augTree.getTree().getNodesAsArray()) {
			if (n.isRoot()) continue; // TODO change for origin edge
			
			if (!n.isLeaf() && !n.isFake()) {
				logP += Math.log(augTree.getSimulatedLambda(n.getNr()));
			}
			if(n.isLeaf() && n.getHeight() > 1e-8) logP += Math.log(augTree.getPsi());

			AugmentedNode subTree = augTree.getAugmentedTree(n.getNr());
			double rootL = augTree.getSimulatedLambda(n.getParent().getNr());
			double rootM = augTree.getSimulatedMu(n.getParent().getNr());
			logP += subTreeLogLik(subTree, rootL, rootM, n.getParent().getHeight(), n.getHeight());

			if (Double.isInfinite(logP)) return logP;
		}
		return logP;
	}

	/**
	 * Gets the log probability of survival under the current process.
	 *
	 * @return the survival log probability
	 */
	private double getSurvivalLogProbability() {
		int ntries = 0;
		int successful = 0;
		while (ntries < 1000 || successful < 1) {
			try {
				if (augTree.simulateSurvival()) successful++;
				ntries++;
			} catch (AugmentedSimulationException e) {} // simulation failed, not counted as trial
		}

		return Math.log((double) successful / (double) ntries);
	}

	/**
	 * Calculate the log likelihood (recursive) of an augmented (sub)tree.
	 *
	 * @param node the root node of the subtree
	 * @param rootL the birth rate above the root
	 * @param rootM the death rate above the root
	 * @param parentHeight the parent height
	 * @param nodeHeight the height of the reconstructed tree node
	 * @return the log likelihood
	 */
	private double subTreeLogLik(AugmentedNode node, double rootL, double rootM, double parentHeight, double nodeHeight) {
		if (node.isSampled && Math.abs(node.getHeight() - nodeHeight) > 1e-6)
			throw new IllegalStateException("Mismatched heights in tree and augmented tree");

		// there is no resampling of rates at fake nodes
		// this factor compensates for the reverse factor wrongly added to the sister of this SA
		if (parentHeight == node.getHeight()) {
			return (-augTree.getLambdaLogLik(rootL, node.lambda) - augTree.getMuLogLik(rootM, node.mu));
		}

		double logLik = augTree.getLambdaLogLik(rootL, node.lambda) + augTree.getMuLogLik(rootM, node.mu);
		logLik -= (node.lambda + node.mu + augTree.getPsi()) * (parentHeight - node.getHeight());

		if (node.isLeaf()) {
			if (node.getHeight() < 1e-8) {
				if (node.isSampled) logLik += Math.log(augTree.getRho());
				else logLik += Math.log(1 - augTree.getRho());
			} else {
				if (!node.isSampled) logLik += Math.log(node.mu);
			}
		} else {
			if (!node.isFake()) logLik += Math.log(node.lambda);
			for (Node ch : node.getChildren()) {
				logLik += subTreeLogLik((AugmentedNode) ch, node.lambda, node.mu, node.getHeight(), nodeHeight);
			}
		}
		return logLik;
	}

	/**
	 * @return whether the likelihood requires recalculation
	 */
	@Override
	protected boolean requiresRecalculation() {
		return augTree.isDirtyCalculation() || augTree.somethingIsDirty();
	}

	/**
	 * Checks if calculation is stochastic.
	 *
	 * @return true, if it is stochastic
	 */
	@Override
	public boolean isStochastic() {
		return conditionOnSurvival.get();
	}

	/**
	 * Gets the non stochastic log likelihood (i.e. without survival).
	 *
	 * @return the calculated log likelihood
	 */
	@Override
	public double getNonStochasticLogP() {
		return calculateLogPHelper(false);
	}
}
