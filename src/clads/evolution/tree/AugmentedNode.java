/*
 * Copyright (C) 2021 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.evolution.tree;

import java.util.List;
import java.util.Locale;

import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;

/**
 * Node for an augmented tree, storing simulated rates.
 */
public class AugmentedNode extends Node {

	// rate metadata:
	public double lambda = 0, mu = 0;
	public boolean isSampled = false;

	/**
	 * Instantiates a new augmented node.
	 */
	public AugmentedNode() {}

	/**
	 * Instantiates a new augmented node.
	 *
	 * @param l the birth rate
	 * @param m the death rate
	 */
	public AugmentedNode(double l, double m) {
		lambda = l;
		mu = m;
	}

	/**
	 * Scale node and children, setting the sampled node to a specific value.
	 *
	 * @param scale the scale factor
	 * @param newSampledHeight the new height of the sampled node
	 * @return the number of degrees of freedom
	 */
	public int scaleWithSampled(double scale, double newSampledHeight) {
		int dof = 0;
		isDirty |= Tree.IS_DIRTY;
		
		if(isLeaf() && height < 1e-8) return 0;
		
		if (!isSampled && !isFake()) {
			height = height * scale;
			dof += 1;
		}
		else if(!Double.isNaN(newSampledHeight)) height = newSampledHeight;

		for (Node ch : getChildren()) {
			dof += ((AugmentedNode) ch).scaleWithSampled(scale, newSampledHeight);
			if (height < ch.getHeight()) {
				throw new IllegalArgumentException("Scale gives negative branch length");
			}
		}

		return dof;
	}

	/**
	 * Scale node and children excluding nodes from the full tree.
	 *
	 * @param scale the scale factor
	 * @return the number of degrees of freedom
	 */
	@Override
	public int scale(final double scale) {
		return scaleWithSampled(scale, Double.NaN);
	}
	
	public int scaleLengthsWithSampled(double scale, double refpoint, double newSampledHeight) {
		int dof = 0;
		isDirty |= Tree.IS_DIRTY;
		
		if(isLeaf() && height < 1e-8 && !isSampled) return 0;
		
		if (!isSampled && !isFake()) {
			height = refpoint + (height - refpoint) * scale;	
			dof += 1;
		}
		else if(!Double.isNaN(newSampledHeight)) height = newSampledHeight;
				
		for (Node ch : getChildren()) {
			dof += ((AugmentedNode) ch).scaleLengthsWithSampled(scale, refpoint, newSampledHeight);
			if (height < ch.getHeight()) {
				throw new IllegalArgumentException("Scale gives negative branch length");
			}
		}

		return dof;
	}

	/**
	 * Shallow copy.
	 *
	 * @return shallow copy of node
	 */
	public AugmentedNode shallowCopy() {
		AugmentedNode node = new AugmentedNode();
		node.height = height;
		node.parent = parent;
		node.children.addAll(children);

		node.lambda = lambda;
		node.mu = mu;
		node.isSampled = isSampled;

		node.labelNr = labelNr;
		node.metaDataString = metaDataString;
		node.ID = ID;

		return node;
	}

	// ************************** Methods ported from Node * **************************.

	/**
	 * Deep copy.
	 *
	 * @return (deep) copy of node
	 */
	@Override
	public AugmentedNode copy() {
		AugmentedNode node = new AugmentedNode();
		node.height = height;
		node.labelNr = labelNr;
		node.metaDataString = metaDataString;
		node.parent = null;
		node.ID = ID;
		node.lambda = lambda;
		node.mu = mu;
		node.isSampled = isSampled;

		for (Node child : getChildren()) {
			node.addChild(child.copy());
		}

		return node;
	}

	/**
	 * Assign values from a tree in array representation.
	 *
	 * @param nodes the nodes in the tree
	 * @param node the specific node to assign
	 */
	@Override
	public void assignFrom(Node[] nodes, Node node) {
		height = node.getHeight();
		labelNr = node.getNr();
		metaDataString = node.metaDataString;
		parent = null;
		ID = node.getID();

		if (node instanceof AugmentedNode) {
			AugmentedNode mtNode = (AugmentedNode) node;
			lambda = mtNode.lambda;
			mu = mtNode.mu;
			isSampled = mtNode.isSampled;
		} else {
			lambda = (double) node.getMetaData("lambda");
			mu = (double) node.getMetaData("mu");
			isSampled = Boolean.parseBoolean((String) node.getMetaData("isSampled"));
			Double h = (Double) node.getMetaData("adjHeight");
			if (h != null) height = h;
		}

		removeAllChildren(false);
		for (Node child : node.getChildren()) {
			addChild(new AugmentedNode());
			getChild(getChildCount() - 1).assignFrom(nodes, child);
			getChild(getChildCount() - 1).setParent(this);
		}
	}

	/**
	 * To short newick.
	 *
	 * @param printInternalNodeNumbers whether to print internal node numbers
	 * @return the Newick string
	 */
	@Override
	public String toShortNewick(final boolean printInternalNodeNumbers) {
		setMetaData("lambda", lambda);
		setMetaData("mu", mu);
		setMetaData("isSampled", isSampled);

		// need to record height for cases where augmented tree is a single edge with end at the sampled node
		metaDataString = String.format(Locale.ENGLISH, "%s=%f,%s=%f,%s=%b,%s=%f", "lambda", lambda, "mu", mu,
				"isSampled", isSampled, "adjHeight", height);

		return super.toShortNewick(printInternalNodeNumbers);
	}

	/**
	 * Assign node numbers.
	 */
	public void assignNodeNumbers() {
		assignNodeNumbersHelper(0);
	}

	/**
	 * Assign node numbers helper.
	 *
	 * @param start the next number to assign
	 * @return the new next number
	 */
	int assignNodeNumbersHelper(int start) {
		this.labelNr = start;
		int nextNr = start + 1;
		for (Node n : getChildren()) {
			nextNr = ((AugmentedNode) n).assignNodeNumbersHelper(nextNr);
		}
		return nextNr;
	}

	// ****************************************************.

	/**
	 * Adds the specified offset to heights.
	 *
	 * @param offset the offset
	 */
	public void addOffsetToHeights(double offset) {
		this.height += offset;
		for (Node n : getChildren()) {
			((AugmentedNode) n).addOffsetToHeights(offset);
		}
	}

	/**
	 * Gets the sampled height.
	 *
	 * @return the sampled height
	 */
	public double getSampledHeight() {
		if (isSampled) return this.height;
		for (Node n : getChildren()) {
			double samph = ((AugmentedNode) n).getSampledHeight();
			if (!Double.isNaN(samph)) return samph;
		}
		return Double.NaN;
	}

	/**
	 * Updates the value of mu using the turnover.
	 *
	 * @param turnover the turnover
	 * @return the number of scale operations done
	 */
	public int updateMuFromTurnover(double turnover) {
		mu = lambda * turnover;
		int dof = 1;
		for (Node n : getChildren()) {
			dof += ((AugmentedNode) n).updateMuFromTurnover(turnover);
		}
		return dof;
	}

	/**
	 * Order the augmented node so that the sampled leaf is on the left side when converted to newick.
	 */
	public void reorderChildren() {
		if (this.getChildCount() == 0) return;
		Node leftChild = this.getLeft();

		if (leftChild.isLeaf()) { // This condition is needed because getAllChildNodesAndSelf returns [] if the node has no child.
			if (!((AugmentedNode) leftChild).isSampled) {
				this.setChild(0, this.getRight());
				this.setChild(1, leftChild);
			}
		} else {
			List<Node> leftChildDescendants = leftChild.getAllChildNodesAndSelf();
			boolean sampledChild = false;
			for (Node ch : leftChildDescendants) {
				if (((AugmentedNode) ch).isSampled) {
					sampledChild = true;
					break;
				}
			}
			if (!sampledChild) {
				this.setChild(0, this.getRight());
				this.setChild(1, leftChild);
			}
		}
		Node nextnode = this.getLeft();
		((AugmentedNode) nextnode).reorderChildren();
	}
}
