/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.evolution.tree;

import java.util.List;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.coalescent.RandomTree;
import beast.base.inference.StateNode;

/**
 * Initializer class for an AugmentedTree, ensures that the tree and augmented tree match in a ClaDS analysis.
 */
public class AugmentedTreeInitializer extends RandomTree {
	public Input<AugmentedTree> augTreeInput = new Input<>("augmentedTree", "Augmented tree to initialize", Validate.REQUIRED);

	/**
	 * Inits the state nodes (i.e. the augmented tree here).
	 */
	@Override
	public void initStateNodes() {
		super.initStateNodes();
		augTreeInput.get().initAndValidate();
	}

	/**
	 * Adds the newly initialised state nodes to the provided list.
	 *
	 * @param stateNodes the list of state nodes
	 */
	@Override
	public void getInitialisedStateNodes(final List<StateNode> stateNodes) {
		super.getInitialisedStateNodes(stateNodes);
		stateNodes.add(augTreeInput.get());
	}
}
