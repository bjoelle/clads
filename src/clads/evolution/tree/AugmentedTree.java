/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.evolution.tree;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.math.distribution.NormalDistributionImpl;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.StateNode;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;

/**
 * Augmented Tree containing simulated full subtrees for each edge of a reconstructed tree.
 */
public class AugmentedTree extends StateNode {

	public Input<RealParameter> lambda0Input = new Input<>("lambda0", "Initial birth rate", Validate.REQUIRED);
	public Input<RealParameter> alphaInput = new Input<>("alpha", "Trend parameter for birth rates", Validate.REQUIRED);
	public Input<RealParameter> sigmaInput = new Input<>("sigma", "Stochasticity of birth rate inheritance", Validate.REQUIRED);

	public Input<Tree> treeInput = new Input<>("tree", "Tree", Validate.REQUIRED);

	public Input<Boolean> useTurnoverInput = new Input<>("useTurnover", "Whether death rates are parametrized using turnover, default true", true);
	public Input<RealParameter> mu0Input = new Input<>("mu0", "Initial death rate");
	public Input<RealParameter> alphaMInput = new Input<>("alphaM", "Trend parameter for death rates");
	public Input<RealParameter> sigmaMInput = new Input<>("sigmaM", "Stochasticity of death rate inheritance");
	public Input<RealParameter> turnoverInput = new Input<>("turnover", "Turnover (= death rate / birth rate)");

	public Input<RealParameter> rhoInput = new Input<>("rho", "Extant sampling probability, default 1.0");
	public Input<RealParameter> psiInput = new Input<>("psi", "Fossil sampling rate");

	// TODO add origin edge & handling

	Tree tree;
	RealParameter origin, lambda0, mu0, alpha, sigma, alphaM, sigmaM, rho, psi, turnover;

	// Augmented simulations
	AugmentedNode[] augmentedTrees;
	AugmentedNode[] stored_augmentedTrees;
	// in order for each node : N of active nodes, lambda_node, mu_node
	double[][] augmentedParameters, stored_augmentedParameters;

	// Simulation variables
	int maxNodes = 1000; // TODO using claDS default, but should probably be set in function of rho ?
	private ArrayList<Double> subtreeLambdas, subtreeMus;
	private ArrayList<AugmentedNode> activeNodes;

	// Storing/restoring variable
	boolean stored = false;

	/**
	 * Instantiates a new augmented tree.
	 */
	public AugmentedTree() {}

	@Override
	public void initAndValidate() {
		tree = treeInput.get();

		lambda0 = lambda0Input.get();
		lambda0.setLower(0.0);
		if (lambda0.getValue() < 0) throw new IllegalArgumentException("Initial birth rate must be positive");

		alpha = alphaInput.get();
		alpha.setLower(0.0);
		if (alpha.getValue() < 0) throw new IllegalArgumentException("Trend parameter alpha must be positive");

		sigma = sigmaInput.get();
		sigma.setLower(0.0);
		if (sigma.getValue() < 0) throw new IllegalArgumentException("Variance parameter sigma must be positive");

		if (isTurnoverUsed()) {
			turnover = turnoverInput.get();
			if (turnover == null) throw new IllegalArgumentException("Turnover parameter must be specified");
			turnover.setLower(0.0);
			if (turnover.getValue() < 0) throw new IllegalArgumentException("Turnover must and be positive");
		} else {
			mu0 = mu0Input.get();
			if (mu0 == null) throw new IllegalArgumentException("Initial death rate must be specified");
			mu0.setLower(0.0);
			if (mu0.getValue() < 0) throw new IllegalArgumentException("Initial death rate must be positive");

			alphaM = alphaMInput.get();
			if (alphaM == null) throw new IllegalArgumentException("Trend parameter alpha M must be specified");
			alphaM.setLower(0.0);
			if (alphaM.getValue() < 0) throw new IllegalArgumentException("Trend parameter alpha M must be positive");

			sigmaM = sigmaMInput.get();
			if (sigmaM == null) throw new IllegalArgumentException("Variance parameter sigmaM must be specified");
			sigmaM.setLower(0.0);
			if (sigmaM.getValue() < 0) throw new IllegalArgumentException("Variance parameter sigmaM must be positive");
		}

		rho = rhoInput.get() != null ? rhoInput.get() : new RealParameter(new Double[] { 1.0 });
		rho.setBounds(0.0, 1.0);
		if (rho.getValue() < 0 || rho.getValue() > 1)
			throw new IllegalArgumentException("Extant sampling proportion must be between 0 and 1");

		psi = psiInput.get() != null ? psiInput.get() : new RealParameter(new Double[] { 0.0 });
		psi.setLower(0.0);
		if (psi.getValue() < 0) throw new IllegalArgumentException("Fossil sampling rate must be positive");
		if (psi.isEstimated() && psi.getValue() == 0) throw new IllegalArgumentException("Initial fossil sampling rate must be > 0");

		maxNodes = Math.max(1000, (int) Math.round(10 / rho.getValue()));

		augmentedTrees = new AugmentedNode[tree.getNodeCount() - 1]; // TODO modify for origin handling
		augmentedParameters = new double[tree.getNodeCount()][3];

		simulateAllAugmentedTrees();
	}

	/**
	 * Simulate all augmented trees (used only for initial simulation).
	 */
	private void simulateAllAugmentedTrees() {
		setSimulatedLambda(tree.getRoot().getNr(), lambda0.getValue());
		setSimulatedMu(tree.getRoot().getNr(), getMu0());
		simulateAugmentedTree(tree.getRoot(), true);
	}

	/**
	 * Simulate augmented tree for edge above the specified node (multiple attempts).
	 *
	 * @param node the node
	 * @param initialSim whether to simulate children nodes too (true if initial simulation)
	 */
	public void simulateAugmentedTree(Node node, boolean initialSim) {
		if (!initialSim) startEditing(null);

		if (!node.isRoot()) {
			Node parent = node.getParent();
			if (initialSim) {
				simulateDefaultTree(node.getNr(), node.getHeight(), getSimulatedLambda(parent.getNr()), getSimulatedMu(parent.getNr()));
			} else {
				if (node.getParent().isRoot() && node.getParent().isFake()) {
					simulateBranchTree(node, parent.getHeight(), node.getHeight(), this.getLambda0(), this.getMu0());
				} else {
					simulateBranchTree(node, parent.getHeight(), node.getHeight(), getSimulatedLambda(parent.getNr()),
							getSimulatedMu(parent.getNr()));
				}				
			}
		}

		if (initialSim) {
			for (Node child : node.getChildren()) {
				simulateAugmentedTree(child, initialSim);
			}
		}
	}

	/**
	 * Simulate augmented tree for edge above the specified node (one attempt).
	 *
	 * @param selected_node the node we are simulating for
	 * @param startTime the start time of the simulation (parent height)
	 * @param endTime the end time of the simulation (node height)
	 * @param rootLambda the root lambda
	 * @param rootMu the root mu
	 */
	private void simulateBranchTree(Node selected_node, double startTime, double endTime, double rootLambda, double rootMu) {
		if (startTime == endTime) {
			AugmentedNode subtreeRoot = new AugmentedNode(rootLambda, rootMu);
			subtreeRoot.setHeight(startTime);
			setSimulatedLambda(selected_node.getNr(), rootLambda);
			setSimulatedMu(selected_node.getNr(), rootMu);
			subtreeRoot.isSampled = true;
			augmentedTrees[selected_node.getNr()] = subtreeRoot;
			return;
		}

		double startLambda, startMu;
		if (selected_node.getParent().isFake()) {
			startLambda = rootLambda;
			startMu = rootMu;
		} else {
			startLambda = sampleLambda(rootLambda);
			startMu = sampleMu(rootMu, startLambda);
		}

		subtreeLambdas = new ArrayList<>(Arrays.asList(startLambda));
		subtreeMus = new ArrayList<>(Arrays.asList(startMu));

		AugmentedNode subtreeRoot = new AugmentedNode(startLambda, startMu);
		activeNodes = new ArrayList<>(Arrays.asList(subtreeRoot));

		boolean extinct = simulateSubTree(startTime, endTime);
		if (extinct) throw new AugmentedSimulationException("Simulation went extinct before finishing time.");

		int selectNode = Randomizer.nextInt(activeNodes.size());
		setSimulatedLambda(selected_node.getNr(), subtreeLambdas.get(selectNode));
		setSimulatedMu(selected_node.getNr(), subtreeMus.get(selectNode));
		setAugmentedActiveNodesN(selected_node.getNr(), activeNodes.size());

		if (!selected_node.isLeaf() || endTime < 1e-8) activeNodes.get(selectNode).isSampled = true;

		if (endTime < 1e-8 && selected_node.isLeaf()) {
			if (rho.getValue() == 1 && activeNodes.size() > 1) throw new AugmentedSimulationException("Simulation incompatible with rho=1");
			for (AugmentedNode node : activeNodes) node.setHeight(0.0);
		} else {
			if (selected_node.isLeaf()) {
				AugmentedNode child1 = new AugmentedNode(subtreeLambdas.get(selectNode), subtreeMus.get(selectNode)),
						child2 = new AugmentedNode(subtreeLambdas.get(selectNode), subtreeMus.get(selectNode));
				activeNodes.get(selectNode).addChild(child1);
				activeNodes.get(selectNode).addChild(child2);
				child1.setParent(activeNodes.get(selectNode));
				child2.setParent(activeNodes.get(selectNode));
				child1.setHeight(endTime);
				child1.isSampled = true;
				activeNodes.add(child2);
				subtreeLambdas.add(child2.lambda);
				subtreeMus.add(child2.mu);
			}
			activeNodes.get(selectNode).setHeight(endTime);
			activeNodes.remove(selectNode);
			subtreeLambdas.remove(selectNode);
			subtreeMus.remove(selectNode);

			if (activeNodes.size() > 0) { // check in case the sampled node was the only survivor
				simulateSubTree(endTime, 0.0);
				if (activeNodes.size() > 0 && rho.getValue() == 1) throw new AugmentedSimulationException("Simulation incompatible with rho=1");
				for (AugmentedNode node : activeNodes) node.setHeight(0.0);
			}
		}
		augmentedTrees[selected_node.getNr()] = subtreeRoot;
	}

	/**
	 * Simulate subtree between two time points.
	 *
	 * @param startTime the start time of the simulation
	 * @param endTime the end time of the simulation
	 * @return true, if simulation went extinct before the end time
	 */
	private boolean simulateSubTree(double startTime, double endTime) {
		double totalRate = subtreeLambdas.stream().mapToDouble(Double::doubleValue).sum()
				+ subtreeMus.stream().mapToDouble(Double::doubleValue).sum();
		double time = Randomizer.nextExponential(totalRate);
		time = startTime - time;

		while (time > endTime) {
			double selector = Randomizer.nextDouble() * totalRate;
			for (int i = 0; i < activeNodes.size(); i++) {
				if (selector - subtreeLambdas.get(i) - subtreeMus.get(i) > 0) {
					selector -= subtreeLambdas.get(i) + subtreeMus.get(i);
					continue;
				}

				Node currentNode = activeNodes.get(i);
				if (selector - subtreeLambdas.get(i) < 0) { // speciation on node i
					double newL1 = sampleLambda(subtreeLambdas.get(i)), newL2 = sampleLambda(subtreeLambdas.get(i));
					if (newL1 == 0 || newL2 == 0) throw new AugmentedSimulationException("Underflow on lambda rate sampling"); // underflow
					double newM1 = sampleMu(subtreeMus.get(i), newL1);
					double newM2 = sampleMu(subtreeMus.get(i), newL2);
					if ((newM1 == 0 || newM2 == 0) && (!isTurnoverUsed() || turnover.getValue() > 0)) {
						throw new AugmentedSimulationException("Underflow on mu rate sampling"); // underflow
					}

					AugmentedNode child1 = new AugmentedNode(newL1, newM1), child2 = new AugmentedNode(newL2, newM2);
					currentNode.addChild(child1);
					currentNode.addChild(child2);
					child1.setParent(currentNode);
					child2.setParent(currentNode);

					activeNodes.addAll(Arrays.asList(child1, child2));
					subtreeLambdas.addAll(Arrays.asList(newL1, newL2));
					subtreeMus.addAll(Arrays.asList(newM1, newM2));
				}

				currentNode.setHeight(time);
				activeNodes.remove(i);
				subtreeLambdas.remove(i);
				subtreeMus.remove(i);

				if (activeNodes.isEmpty()) return true;
				if (activeNodes.size() > maxNodes) throw new OversizeSimulationException();
				break;
			}

			totalRate = subtreeLambdas.stream().mapToDouble(Double::doubleValue).sum()
					+ subtreeMus.stream().mapToDouble(Double::doubleValue).sum();
			if (Double.isInfinite(totalRate)) throw new AugmentedSimulationException("Overflow on total rate");
			double newtime = Randomizer.nextExponential(totalRate);
			time = time - newtime;
		}

		return false;
	}

	/**
	 * Simulate default augmented tree (no events) for edge above the specified node.
	 *
	 * @param nodeNr the node nr
	 * @param endTime the end time of the simulation (node height)
	 * @param rootLambda the root lambda
	 * @param rootMu the root mu
	 */
	private void simulateDefaultTree(int nodeNr, double endTime, double rootLambda, double rootMu) {
		double startLambda = sampleLambda(rootLambda);
		double startMu = sampleMu(rootMu, startLambda);
		if (tree.getNode(nodeNr).getParent().isFake()) {
			startLambda = rootLambda;
			startMu = rootMu;
		}
		AugmentedNode subtreeRoot = new AugmentedNode(startLambda, startMu);

		setSimulatedLambda(nodeNr, startLambda);
		setSimulatedMu(nodeNr, startMu);
		setAugmentedActiveNodesN(nodeNr, 1);

		subtreeRoot.isSampled = true;
		subtreeRoot.setHeight(endTime);
		augmentedTrees[nodeNr] = subtreeRoot;
	}

	/**
	 * Simulate a survival trial, i.e. whether two lineages started at the root age under the current process both survive.
	 *
	 * @return true, if process survived
	 */
	public boolean simulateSurvival() {
		for (int i = 0; i < 2; i++) { // TODO change for root condition vs survival
			double startLambda = sampleLambda(getLambda0());
			subtreeLambdas = new ArrayList<>(Arrays.asList(startLambda));
			double startMu = sampleMu(getMu0(), startLambda);
			subtreeMus = new ArrayList<>(Arrays.asList(startMu));

			AugmentedNode subtreeRoot = new AugmentedNode(startLambda, startMu);
			activeNodes = new ArrayList<>(Arrays.asList(subtreeRoot));

			boolean extinct;
			try { // TODO set max nodes higher here ?
				extinct = simulateSubTree(tree.getRoot().getHeight(), 0);
			} catch (OversizeSimulationException e) {
				extinct = false; // if simulation is over size, unlikely to go extinct
			}
			if (extinct) return false;
		}

		return true;
	}

	/**
	 * Sample a new birth rate.
	 *
	 * @param originLambda the rate before sampling
	 * @return the new rate
	 */
	private double sampleLambda(double originLambda) {
		double mu = Math.log(originLambda * alpha.getValue());
		return Randomizer.nextLogNormal(mu, sigma.getValue(), false);
	}

	/**
	 * Sample a new death rate.
	 *
	 * @param originMu the rate before sampling, used if mus are specified using a distribution
	 * @param newLambda the corresponding new birth rate, used if mus are specified using turnover
	 * @return the new rate
	 */
	private double sampleMu(double originMu, double newLambda) {
		if (isTurnoverUsed()) return newLambda * turnover.getValue();

		double mu = Math.log(originMu * alphaM.getValue());
		return Randomizer.nextLogNormal(mu, sigmaM.getValue(), false);
	}

	/**
	 * Gets the corresponding (reconstructed) tree.
	 *
	 * @return the tree
	 */
	public Tree getTree() {
		return tree;
	}

	/**
	 * Gets the augmented tree for the specified node.
	 *
	 * @param nr the node nr
	 * @return the augmented tree
	 */
	public AugmentedNode getAugmentedTree(int nr) {
		return augmentedTrees[nr];
	}

	/**
	 * Gets the number of active nodes in the augmented tree at time of node.
	 *
	 * @param nr the node nr
	 * @return the number of active nodes
	 */
	public double getAugmentedActiveNodesN(int nr) {
		return augmentedParameters[nr][0];
	}

	/**
	 * Sets the number of active nodes in the augmented tree at time of node.
	 *
	 * @param nr the node nr
	 * @param value the new number
	 */
	private void setAugmentedActiveNodesN(int nr, double value) {
		augmentedParameters[nr][0] = value;
	}

	/**
	 * Gets the simulated birth rate for the specified node.
	 *
	 * @param nr the node nr
	 * @return the simulated lambda
	 */
	public double getSimulatedLambda(int nr) {
		return augmentedParameters[nr][1];
	}

	/**
	 * Sets the simulated birth rate for the specified node.
	 *
	 * @param nr the node nr
	 * @param value the new lambda
	 */
	private void setSimulatedLambda(int nr, double value) {
		augmentedParameters[nr][1] = value;
	}

	/**
	 * Gets the simulated death rate for the specified node.
	 *
	 * @param nr the node nr
	 * @return the simulated mu
	 */
	public double getSimulatedMu(int nr) {
		if (!isTurnoverUsed()) return augmentedParameters[nr][2];
		return getSimulatedLambda(nr) * turnover.getValue();
	}

	/**
	 * Sets the simulated death rate for the specified node.
	 *
	 * @param nr the node nr
	 * @param value the new mu
	 */
	private void setSimulatedMu(int nr, double value) {
		if (!isTurnoverUsed()) augmentedParameters[nr][2] = value;
	}

	/**
	 * Gets the log likelihood of sampling a new lambda from the previous.
	 *
	 * @param upLambda the previous lambda
	 * @param downLambda the sampled lambda
	 * @return the log likelihood
	 */
	public double getLambdaLogLik(double upLambda, double downLambda) {
		if (downLambda <= 0 || upLambda <= 0) return Double.NEGATIVE_INFINITY;
		double mu = Math.log(upLambda * alpha.getValue());
		if (sigma.getValue() == 0) {
			if (Math.abs(mu - Math.log(downLambda)) < 1e-5) return 0;
			return Double.NEGATIVE_INFINITY;
		}
		return new NormalDistributionImpl(mu, sigma.getValue()).logDensity(Math.log(downLambda)) - Math.log(downLambda);
	}

	/**
	 * Gets the log likelihood of sampling a new mu from the previous.
	 *
	 * @param upMu the previous mu
	 * @param downMu the sampled mu
	 * @return the log likelihood
	 */
	public double getMuLogLik(double upMu, double downMu) {
		if (isTurnoverUsed()) return 0;

		double mu = Math.log(upMu * alphaM.getValue());
		if (sigmaM.getValue() == 0) {
			if (Math.abs(mu - Math.log(downMu)) < 1e-5) return 0;
			return Double.NEGATIVE_INFINITY;
		}
		return new NormalDistributionImpl(mu, sigmaM.getValue()).logDensity(Math.log(downMu)) - Math.log(downMu);
	}

	/**
	 * Checks if death rate parametrization uses turnover.
	 *
	 * @return true, if turnover is used
	 */
	public boolean isTurnoverUsed() {
		return useTurnoverInput.get();
	}

	/**
	 * @return whether the augmented tree requires recalculation
	 */
	// CalculationNode overrides
	@Override
	protected boolean requiresRecalculation() {
		if (tree.somethingIsDirty() || lambda0.somethingIsDirty() || alpha.somethingIsDirty() || sigma.somethingIsDirty()
				|| rho.somethingIsDirty() || psi.somethingIsDirty()) {
			return true;
		}
		if (isTurnoverUsed() && turnover.somethingIsDirty()) {
			return true;
		}
		if (!isTurnoverUsed() && (mu0.somethingIsDirty() || alphaM.somethingIsDirty() || sigmaM.somethingIsDirty())) {
			return true;
		}

		return hasStartedEditing;
	}

	@Override
	public void store() {
		if (stored) return;

		this.stored_augmentedParameters = new double[augmentedParameters.length][3];
		for (int i = 0; i < augmentedParameters.length; i++) {
			stored_augmentedParameters[i] = augmentedParameters[i].clone();
		}

		this.stored_augmentedTrees = new AugmentedNode[augmentedTrees.length];
		for (int i = 0; i < augmentedTrees.length; i++) {
			stored_augmentedTrees[i] = augmentedTrees[i].copy();
		}

		stored = true;
	}

	@Override
	public void restore() {
		augmentedParameters = stored_augmentedParameters;
		augmentedTrees = stored_augmentedTrees;

		stored = false;
	}

	@Override
	public void accept() {
		stored = false;
		super.accept();
	}

	// Parameter getters & setters
	/**
	 * Gets the initial lambda at the root of the process.
	 *
	 * @return the initial lambda
	 */
	public double getLambda0() {
		return lambda0.getValue();
	}

	/**
	 * Gets the initial mu at the root of the process.
	 *
	 * @return the initial mu
	 */
	public double getMu0() {
		return isTurnoverUsed() ? lambda0.getValue() * turnover.getValue() : mu0.getValue();
	}

	/**
	 * Gets the sampling probability at present rho.
	 *
	 * @return rho
	 */
	public double getRho() {
		return rho.getValue();
	}

	/**
	 * Gets the fossil sampling rate psi.
	 *
	 * @return psi
	 */
	public double getPsi() {
		return psi.getValue();
	}

	/**
	 * Update rates at the root of the process.
	 */
	public void updateRates() { // update for root edge
		setSimulatedLambda(tree.getRoot().getNr(), getLambda0());
		setSimulatedMu(tree.getRoot().getNr(), getMu0());
		if (isTurnoverUsed()) {
			for (AugmentedNode augT : augmentedTrees) {
				augT.updateMuFromTurnover(turnover.getValue());
			}
		}
	}

	/**
	 * Update turnover in augmented trees.
	 *
	 * @param newValue the new value of the turnover
	 * @return the number of scale operations done
	 */
	public int updateTurnover(double newValue) {
		startEditing(null);
		int dof = 0;
		for (AugmentedNode augT : augmentedTrees) {
			dof += augT.updateMuFromTurnover(turnover.getValue());
		}
		return dof;
	}

	@Override
	public int scale(double scale) {
		throw new UnsupportedOperationException();
	}

	/**
	 * OversizeSimulationException is thrown when the simulation reaches the max number of nodes.
	 */
	protected class OversizeSimulationException extends AugmentedSimulationException {

		public OversizeSimulationException() {
			super("Reached max nodes");
		}

		private static final long serialVersionUID = -8031746131718986762L;
	}

	/**
	 * AugmentedSimulationException is thrown when the augmented tree simulation fails.
	 */
	public class AugmentedSimulationException extends IllegalStateException {

		private static final long serialVersionUID = -4842716672022308062L;

		/**
		 * Instantiates a new augmented simulation exception.
		 *
		 * @param string the description of the failure
		 */
		public AugmentedSimulationException(String string) {
			super(string);
		}
	}

	// Loggable implementations

	/**
	 * Inits the logging stream.
	 *
	 * @param out the out stream
	 */
	@Override
	public void init(PrintStream out) {
		for (int value = 0; value < augmentedTrees.length; value++) {
			out.print(getID() + ".node.tree." + value + "\t");
		}
		for (int value = 0; value < augmentedParameters.length; value++) {
			out.print(getID() + ".node.lambda." + value + "\t");
			out.print(getID() + ".node.mu." + value + "\t");
		}
	}

	/**
	 * Log one sample to stream.
	 *
	 * @param sample the sample number
	 * @param out the out stream
	 */
	@Override
	public void log(long sample, PrintStream out) {
		for (AugmentedNode augmentedTree : augmentedTrees) {
			out.print(augmentedTree.toShortNewick(true) + "\t");
		}

		for (int value = 0; value < augmentedParameters.length; value++) {
			out.print(getSimulatedLambda(value) + "\t");
			out.print(getSimulatedMu(value) + "\t");
		}
	}

	/**
	 * Close the stream.
	 *
	 * @param out the out stream
	 */
	@Override
	public void close(PrintStream out) {}

	// StateNode implementations
	/**
	 * Gets the dimension.
	 *
	 * @return the dimension
	 */
	@Override
	public int getDimension() {
		return tree.getDimension();
	}

	/**
	 * Gets the array value.
	 *
	 * @param dim the index of the array
	 * @return the array value
	 */
	@Override
	public double getArrayValue(int dim) {
		return tree.getArrayValue(dim);
	}

	/**
	 * Sets everything dirty.
	 *
	 * @param isDirty the new dirty flag
	 */
	@Override
	public void setEverythingDirty(boolean isDirty) {
		this.setSomethingIsDirty(isDirty);
	}

	/**
	 * Deep copy of the augmented tree.
	 *
	 * @return the copied augmented tree
	 */
	@Override
	public AugmentedTree copy() {
		AugmentedTree copy = new AugmentedTree();
		copy.ID = ID;
		copy.maxNodes = maxNodes;

		copy.augmentedTrees = new AugmentedNode[augmentedTrees.length];
		for (int i = 0; i < augmentedTrees.length; i++) {
			copy.augmentedTrees[i] = augmentedTrees[i].copy();
		}
		copy.augmentedParameters = new double[augmentedParameters.length][3];
		for (int i = 0; i < augmentedParameters.length; i++) {
			copy.augmentedParameters[i] = augmentedParameters[i].clone();
		}

		return copy;
	}

	/**
	 * Assign to another augmented tree.
	 *
	 * @param other the other tree
	 */
	@Override
	public void assignTo(StateNode other) {
		AugmentedTree otherTree = (AugmentedTree) other;

		otherTree.ID = ID;
		otherTree.maxNodes = maxNodes;

		otherTree.augmentedTrees = new AugmentedNode[augmentedTrees.length];
		for (int i = 0; i < augmentedTrees.length; i++) {
			otherTree.augmentedTrees[i] = augmentedTrees[i].copy();
		}
		otherTree.augmentedParameters = new double[augmentedParameters.length][3];
		for (int i = 0; i < augmentedParameters.length; i++) {
			otherTree.augmentedParameters[i] = augmentedParameters[i].clone();
		}
	}

	/**
	 * Assign from another augmented tree.
	 *
	 * @param other the other tree
	 */
	@Override
	public void assignFrom(StateNode other) {
		AugmentedTree otherTree = (AugmentedTree) other;

		ID = otherTree.ID;
		maxNodes = otherTree.maxNodes;

		augmentedTrees = new AugmentedNode[otherTree.augmentedTrees.length];
		for (int i = 0; i < augmentedTrees.length; i++) {
			augmentedTrees[i] = otherTree.augmentedTrees[i].copy();
		}
		augmentedParameters = new double[otherTree.augmentedParameters.length][3];
		for (int i = 0; i < augmentedParameters.length; i++) {
			augmentedParameters[i] = otherTree.augmentedParameters[i].clone();
		}
	}

	/**
	 * Assign from another augmented tree, without IDs.
	 *
	 * @param other the other tree
	 */
	@Override
	public void assignFromFragile(StateNode other) {
		AugmentedTree otherTree = (AugmentedTree) other;

		augmentedTrees = new AugmentedNode[otherTree.augmentedTrees.length];
		for (int i = 0; i < augmentedTrees.length; i++) {
			augmentedTrees[i] = otherTree.augmentedTrees[i].copy();

			double offset = tree.getNode(i).getHeight() - augmentedTrees[i].getSampledHeight();
			augmentedTrees[i].addOffsetToHeights(offset);
		}
		augmentedParameters = new double[otherTree.augmentedParameters.length][3];
		for (int i = 0; i < augmentedParameters.length; i++) {
			augmentedParameters[i] = otherTree.augmentedParameters[i].clone();
		}
	}

	/**
	 * Restore augmented tree from XML state file.
	 *
	 * @param node the XML node
	 */
	@Override
	public void fromXML(org.w3c.dom.Node node) {
		String str = node.getTextContent();

		Pattern pattern = Pattern.compile("^" + " *Trees: *\\[(.*)] *," + " *Values: *\\[([^]]*)] *$");
		Matcher matcher = pattern.matcher(str);

		if (!matcher.find()) throw new RuntimeException("Error parsing AugmentedTree state string.");

		String[] trees = matcher.group(1).split("\t");
		augmentedTrees = new AugmentedNode[trees.length];
		for (int i = 0; i < trees.length; i++) {
			TreeParser parser = new TreeParser();
			parser.initByName("IsLabelledNewick", false, "offset", 0, "adjustTipHeights", false, "newick", trees[i]);

			augmentedTrees[i] = new AugmentedNode();
			augmentedTrees[i].assignFrom(parser.getNodesAsArray(), parser.getRoot());
		}

		String[] values = matcher.group(2).split("\t");
		if (values.length != trees.length + 1) throw new RuntimeException("Error parsing AugmentedTree state string.");

		augmentedParameters = new double[values.length][3];
		for (int i = 0; i < values.length; i++) {
			values[i] = values[i].substring(1, values[i].length() - 1);
			String[] sep_values = values[i].split(";");
			for (int j = 0; j < 3; j++) augmentedParameters[i][j] = Double.parseDouble(sep_values[j]);
		}
	}

	/**
	 * Output augmented tree as arrays to string (for state storing).
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();

		output.append("Trees: [");
		for (int i = 0; i < augmentedTrees.length; i++) {
			if (i > 0) output.append("\t");
			augmentedTrees[i].assignNodeNumbers();
			output.append(augmentedTrees[i].toShortNewick(true));
		}
		output.append("], Values: [");
		for (int i = 0; i < augmentedParameters.length; i++) {
			if (i > 0) output.append("\t");
			output.append("(");
			for (int j = 0; j < 3; j++) {
				if (j > 0) output.append(";");
				output.append(augmentedParameters[i][j]);
			}
			output.append(")");
		}
		output.append("]");

		return output.toString();
	}

	/**
	 * Convert the full (assembled) augmented tree to Newick format.
	 *
	 * @return the Newick representation of the augmented tree
	 */
	public String assembleToNewickFormat() {
		// renumbering all augmented trees to be consistent with the observed tree, 
		// here focusing on the leaves
		int augmentedNodesNr = tree.getLeafNodeCount() + 1;
		for (int i = 0; i < augmentedTrees.length; i++) {
			AugmentedNode augTreeRoot = augmentedTrees[i];
			List<Node> augNodes = augTreeRoot.getAllChildNodesAndSelf();
			if (augNodes.size() == 0) augNodes.add(augTreeRoot); // workaround for getAllChildNodesAndSelf

			for (Node n : augNodes) {
				if (!n.isLeaf()) continue;
				if (tree.getNode(i).isLeaf() && ((AugmentedNode) n).isSampled) {
					// node is a sampled leaf in the final tree
					n.setNr(i + 1);
				} else if (!((AugmentedNode) n).isSampled) { // no renumbering the internal nodes of the observed tree.
					n.setNr(augmentedNodesNr);
					augmentedNodesNr++;
				}
			}
		}
		// here focusing on the internal nodes

		int root_number = augmentedNodesNr;
		augmentedNodesNr++;
		for (int i = 0; i < augmentedTrees.length; i++) {
			AugmentedNode augTreeRoot = augmentedTrees[i];

			List<Node> augNodes = augTreeRoot.getAllChildNodesAndSelf();
			if (augNodes.size() == 0) augNodes.add(augTreeRoot); // workaround for getAllChildNodesAndSelf
			for (Node n : augNodes) {
				if (!n.isLeaf() || (!tree.getNode(i).isLeaf() && ((AugmentedNode) n).isSampled)) {
					n.setNr(augmentedNodesNr);
					augmentedNodesNr++;
				}
			}
		}

		// replacing all nodes in the observed tree with the corresponding augmented subtree
		String observedTree = tree.toString();
		HashMap<String, String> replacements = new HashMap<String, String>();

		Matcher nodeMatches = Pattern.compile("([0-9]+):[0-9]+\\.[0-9]+").matcher(observedTree);
		while (nodeMatches.find()) {
			int currentNode = Integer.parseInt(nodeMatches.group(1));
			if (currentNode == augmentedTrees.length) break;

			// We reorder each subtree so that the sampled leaf is on the leftmost side of the string
			augmentedTrees[currentNode].reorderChildren();
			String augmentedTreeNW = augmentedTrees[currentNode].toShortNewick(true);

			// updating the root branch length
			double root_length = tree.getNode(currentNode).getParent().getHeight() - augmentedTrees[currentNode].getHeight();
			Matcher rootMatch = Pattern.compile(":[0-9]+\\.[0-9]+$").matcher(augmentedTreeNW);
			augmentedTreeNW = rootMatch.replaceFirst(":" + Double.toString(root_length));

			replacements.put(nodeMatches.group(0), augmentedTreeNW);
		}

		for (Entry<String, String> replace : replacements.entrySet()) {
			observedTree = observedTree.replace(replace.getKey(), replace.getValue());
		}

		// we need to sort out the parentheses due to the diversification of internal branches.
		Matcher nodeIssues = Pattern.compile("\\)(\\(+)").matcher(observedTree);
		while (nodeIssues.find()) {
			int numberofparentheses = nodeIssues.group(1).length();
			observedTree = nodeIssues.replaceFirst(")");

			int rankfinder = nodeIssues.start() - 1;
			int rankinparentheses = 1;
			while (rankinparentheses != 0) {
				if (observedTree.charAt(rankfinder) == ')') rankinparentheses++;
				if (observedTree.charAt(rankfinder) == '(') rankinparentheses--;
				rankfinder--;
			}

			String parentheses = "(".repeat(numberofparentheses);
			if (rankfinder != 0) {
				observedTree = observedTree.substring(0, rankfinder + 1) + parentheses + observedTree.substring(rankfinder + 1);
			} else {
				observedTree = parentheses + observedTree;
			}
			nodeIssues = Pattern.compile("\\)(\\(+)").matcher(observedTree);
		}

		Matcher rootMatch = Pattern.compile("[0-9]+:[0-9]+\\.[0-9]+$").matcher(observedTree);
		observedTree = rootMatch.replaceFirst(Integer.toString(root_number) + ":0;");
		for (int i = 1; i < 30; i++) {
			String toreplace = "E-" + Integer.toString(i);
			observedTree = observedTree.replace(toreplace + toreplace, toreplace);
		}

		return observedTree;
	}

	/**
	 * Get all direct descendants (i.e. successive fake nodes) of the node.
	 *
	 * @param node the node
	 * @return the list of direct descendants
	 */
	public ArrayList<Node> getDirectDescendants(Node node) {
		ArrayList<Node> DirectDescendants = new ArrayList<>(node.getChildren());
		Node daughter = node.getNonDirectAncestorChild();
		if (daughter.isFake()) DirectDescendants.addAll(this.getDirectDescendants(daughter));
		return DirectDescendants;
	}

	/**
	 * Simulate the augmented tree for a node and all its direct descendants.
	 *
	 * @param node the selected node
	 */
	public void simulateNodeandDescendants(Node node) {
		this.simulateAugmentedTree(node, false);
		if (node.isFake()) {
			ArrayList<Node> Node_descendants = this.getDirectDescendants(node);
			for (Node n : Node_descendants) this.simulateAugmentedTree(n, false);
		}
	}
}
