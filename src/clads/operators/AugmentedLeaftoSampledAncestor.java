/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

/**
 * LeaftoSample operator for augmented trees. Allows to switch a direct ancestor to a distinct species and vice versa.
 * Adapted from the SA package, original author Alexandra Gavryushkina.
 */
public class AugmentedLeaftoSampledAncestor extends AugmentedTreeOperator {

	boolean areFossilPresent = false;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		for (Node n : tree.getExternalNodes()) {
			if (n.getHeight() > 1e-8) {
				areFossilPresent = true;
				break;
			}
		}
	}

	/**
	 * Perform a proposal switching from SA to leaf or vice-versa.
	 *
	 * @return the logHR of the proposal
	 */
	@Override
	public double proposal() {
		// this should never happen if the analysis is set up properly
		if (!areFossilPresent) return Double.NEGATIVE_INFINITY;

		double logHR = 0.0;
		Node node;
		do {
			node = tree.getNode(Randomizer.nextInt(tree.getLeafNodeCount()));
		} while (node.getHeight() < 1e-8);

		Node parent = node.getParent();
		Node sister = this.getOtherChild(parent, node);
		double newRange = 1, oldRange = 1;

		if (node.isDirectAncestor()) {
			double newHeight;
			if (parent.isRoot()) {
				logHR -= augTreeLogHR(sister) + augTreeLogHR(node);
				double randomNumber = Randomizer.nextExponential(1);
				newHeight = parent.getHeight() + randomNumber;
				newRange = Math.exp(randomNumber);
			} else {
				logHR -= augTreeLogHR(parent);
				// Determination of the new node height:
				newRange = parent.getParent().getHeight() - parent.getHeight();
				newHeight = parent.getHeight() + Randomizer.nextDouble() * newRange;
			}
			parent.setHeight(newHeight);
		} else {
			if (node.getHeight() < sister.getHeight()) {
				return Double.NEGATIVE_INFINITY;
			}
			logHR -= augTreeLogHR(sister) + augTreeLogHR(node);
			if (parent.isRoot()) oldRange = Math.exp(parent.getHeight() - node.getHeight());
			else {
				logHR -= augTreeLogHR(parent);
				oldRange = parent.getParent().getHeight() - node.getHeight();
			}
			parent.setHeight(node.getHeight());
		}
		logHR += Math.log(newRange / oldRange);

		try {
			augTree.simulateAugmentedTree(parent, false);
			augTree.simulateNodeandDescendants(node);
			augTree.simulateNodeandDescendants(sister);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}

		if (node.isDirectAncestor()) {
			if (parent.isRoot()) logHR += augTreeLogHR(sister) + augTreeLogHR(node);
			else logHR += augTreeLogHR(parent);
		} else {
			logHR += augTreeLogHR(sister) + augTreeLogHR(node);
			if (!parent.isRoot()) logHR += augTreeLogHR(parent);
		}

		return logHR;
	}
}