/*
 * Copyright (C) 2021 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

/**
 * Subtree exchange operator for augmented trees.
 * Adapted from the MultiTypeTree package, original author Tim Vaughan.
 */
public class AugmentedSubtreeExchange extends AugmentedTreeOperator {

	public Input<Boolean> isNarrowInput = new Input<>("isNarrow", "Whether or not to use narrow exchange. (Default true.)", true);

	/**
	 * Perform proposal.
	 *
	 * @return the log HR
	 */
	@Override
	public double proposal() {
		double logHR = 0.0;

		// Select source and destination nodes:
		Node srcNode, srcNodeParent, destNode, destNodeParent;
		if (tree.getNodeCount() < 4 || tree.getInternalNodeCount() - tree.getDirectAncestorNodeCount() < 3) {
			return Double.NEGATIVE_INFINITY;
		}

		if (isNarrowInput.get()) {
			// Narrow exchange selection:
			do {
				srcNode = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
			} while (srcNode.isRoot() || srcNode.getParent().isRoot() || srcNode.isDirectAncestor());
			srcNodeParent = srcNode.getParent();
			destNode = getOtherChild(srcNodeParent.getParent(), srcNodeParent);
		} else {
			// Wide exchange selection:
			do {
				srcNode = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
			} while (srcNode.isRoot() || srcNode.isDirectAncestor());
			srcNodeParent = srcNode.getParent();

			do {
				destNode = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
			} while (destNode == srcNode || destNode.isRoot() || destNode.getParent() == srcNode.getParent()
					|| destNode.isDirectAncestor());
		}
		destNodeParent = destNode.getParent();

		// Reject if substitution would result in negative branch lengths:
		if (destNode.getHeight() > srcNodeParent.getHeight() || srcNode.getHeight() > destNodeParent.getHeight()) {
			return Double.NEGATIVE_INFINITY;
		}

		// Record HR of old augmented trees:
		logHR -= augTreeLogHR(srcNode) + augTreeLogHR(destNode);

		// Make changes to tree topology:
		replace(srcNodeParent, srcNode, destNode);
		replace(destNodeParent, destNode, srcNode);

		// Re-simulate augmented trees involved:
		try {
			augTree.simulateNodeandDescendants(srcNode);
			augTree.simulateNodeandDescendants(destNode);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// Record HR of new augmented trees:
		logHR += augTreeLogHR(srcNode) + augTreeLogHR(destNode);
		return logHR;
	}

}
