/*
 *
 * Copyright (C) 2010 Remco Bouckaert remco@cs.auckland.ac.nz
 *
 * This file is part of BEAST2.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership and licensing.
 *
 * BEAST is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 *  BEAST is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with BEAST; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 */
package clads.operators;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.operator.kernel.BactrianScaleOperator;
import beast.base.evolution.tree.Node;
import clads.evolution.tree.AugmentedNode;
import clads.evolution.tree.AugmentedTree;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;


@Description("Scales a parameter and updates the augmented tree.")
public class AugmentedParameterScale extends BactrianScaleOperator {

	public Input<AugmentedTree> augTreeInput = new Input<>("augmentedTree", "Augmented tree on which to operate.", Validate.REQUIRED);

	protected AugmentedTree augTree;

	@Override
	public void initAndValidate() {
		super.initAndValidate();
		augTree = augTreeInput.get();
		if(treeInput.get() != null) throw new IllegalArgumentException("Augmented scaler cannot be used to scale trees");
	}

	/**
	 * propose a scaling move.
	 *
	 * @return log of Hastings Ratio, or Double.NEGATIVE_INFINITY if proposal should not be accepted *
	 */
	@Override
	public double proposal() {
		double logHR = 0;

		Node root = augTree.getTree().getRoot();
		if(root.isFake()) {
			for(Node ch : root.getChildren()) {
				logHR -= augTreeLogHR(ch);
			}
		}

		logHR += super.proposal();
		if(Double.isInfinite(logHR)) return logHR;

		if(root.isFake()) {
			try {
				for(Node ch : root.getChildren()) {
					augTree.simulateNodeandDescendants(ch);
					logHR += augTreeLogHR(ch);
				}
			} catch (AugmentedSimulationException e) {
				return Double.NEGATIVE_INFINITY;
			}
		}
		return logHR;
	}


	/**
	 * Get the log HR for an augmented subtree and potentially of all the direct descendants of the node.
	 *
	 * @param n the node corresponding to the subtree
	 * @return the log HR
	 */
	protected double augTreeLogHR(Node n) {
		augTree.updateRates();
		AugmentedNode root = augTree.getAugmentedTree(n.getNr());
		double rootL = augTree.getSimulatedLambda(n.getParent().getNr());
		double rootM = augTree.getSimulatedMu(n.getParent().getNr());

		double logHR = Math.log(augTree.getAugmentedActiveNodesN(n.getNr()));

		logHR -= subTreeLogLik(root, rootL, rootM, n.getParent().getHeight());
		if (n.isFake()) {
			for (Node ch : n.getChildren()) {
				logHR += augTreeLogHR(ch);
			}
		}
		return logHR;
	}

	/**
	 * Recursive log likelihood of an augmented subtree simulation (minus sampling).
	 *
	 * @param node the root node of the subtree
	 * @param rootL the birth rate above the root
	 * @param rootM the death rate above the root
	 * @param parentHeight the parent height
	 * @return the log likelihood
	 */
	private double subTreeLogLik(AugmentedNode node, double rootL, double rootM, double parentHeight) {
		if (parentHeight == node.getHeight()) {
			return -augTree.getLambdaLogLik(rootL, node.lambda) - augTree.getMuLogLik(rootM, node.mu);
		}
		double logLik = augTree.getLambdaLogLik(rootL, node.lambda) + augTree.getMuLogLik(rootM, node.mu);
		logLik -= (node.lambda + node.mu) * (parentHeight - node.getHeight());

		if (node.isLeaf() && node.getHeight() > 1e-8 && !node.isSampled) {
			logLik += Math.log(node.mu);
		}
		if (!node.isLeaf()) {
			if (!node.isFake()) {
				logLik += Math.log(node.lambda);
			}
			for (Node ch : node.getChildren()) {
				logLik += subTreeLogLik((AugmentedNode) ch, node.lambda, node.mu, node.getHeight());
			}
		}
		return logLik;
	}
}
