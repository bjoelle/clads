package clads.operators;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

public class ANSResim extends AugmentedNodeShift {
	
	@Override
	double rootProposal(Node root) {
		double logHR = 0.0;

		// Get HRs for previous augmented trees
		for (Node ch : root.getChildren()) {
			logHR -= augTreeLogHR(ch);
		}

		// Select new root height:
		double u = Randomizer.nextDouble();
		double f = u * rootScaleFactorInput.get() + (1 - u) / rootScaleFactorInput.get();
		double oldestChildHeight = Math.max(root.getLeft().getHeight(), root.getRight().getHeight());
		root.setHeight(oldestChildHeight + f * (root.getHeight() - oldestChildHeight));

		// Update children augmented trees:
		for (Node ch : root.getChildren()) {
			try {
				augTree.simulateNodeandDescendants(ch);
			} catch (AugmentedSimulationException e) {
				return Double.NEGATIVE_INFINITY;
			}
			logHR += augTreeLogHR(ch);
		}
		logHR -= Math.log(f);
		return logHR;
	}

	@Override
	double nonRootProposal(Node node) {
		double logHR = 0.0;

		// Get HRs for previous augmented trees
		logHR -= augTreeLogHR(node);
		for (Node ch : node.getChildren()) logHR -= augTreeLogHR(ch);

		// Select new node height:
		double upperBound = node.getParent().getHeight();
		double lowerBound = Math.max(node.getLeft().getHeight(), node.getRight().getHeight());
		node.setHeight(lowerBound + (upperBound - lowerBound) * Randomizer.nextDouble());

		// Update node & children augmented trees
		try {
			augTree.simulateAugmentedTree(node, false);
			for (Node ch : node.getChildren()) augTree.simulateNodeandDescendants(ch);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}
		logHR += augTreeLogHR(node);
		for (Node ch : node.getChildren()) logHR += augTreeLogHR(ch);

		return logHR;
	}
}
