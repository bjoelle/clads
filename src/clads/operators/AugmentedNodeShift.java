/*
 * Copyright (C) 2021 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.core.Input;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;

/**
 * Change the height of a random node and rescale the corresponding augmented trees.
 */
public abstract class AugmentedNodeShift extends AugmentedTreeOperator {

	public Input<Boolean> rootOnlyInput = new Input<>("rootOnly", "Always select root node for height adjustment.", false);

	public Input<Boolean> noRootInput = new Input<>("noRoot", "Never select root node for height adjustment.", false);

	public Input<Double> rootScaleFactorInput = new Input<>("rootScaleFactor", "Scale factor used in root height proposals. (Default 0.8)", 0.8);


	@Override
	public void initAndValidate() {
		super.initAndValidate();

		if (rootOnlyInput.get() && noRootInput.get())
			throw new IllegalArgumentException("rootOnly and noRoot inputs " + "cannot both be set to true simultaneously.");
	}

	/**
	 * Perform proposal.
	 *
	 * @return the log HR of the proposed move
	 */
	@Override
	public double proposal() {
		// Select internal node to adjust:
		Node node;
		if (rootOnlyInput.get()) node = tree.getRoot();
		else do {
			node = tree.getNode(tree.getLeafNodeCount() + Randomizer.nextInt(tree.getInternalNodeCount()));
		} while (noRootInput.get() && node.isRoot() || node.isLeaf() || node.isFake());
		// Generate relevant proposal:
		if (node.isRoot()) return rootProposal(node);
		return nonRootProposal(node);
	}

	/**
	 * Proposal applied to the root.
	 *
	 * @param root the root node
	 * @return log HR
	 */
	abstract double rootProposal(Node root);

	/**
	 * Proposal applied to a non-root node.
	 *
	 * @param node the node
	 * @return log HR
	 */
	abstract double nonRootProposal(Node node);

}
