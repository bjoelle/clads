/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

/**
 * Wilson-Balding operator for augmented trees.
 * Adapted from the MultiTypeTree package, original author Tim Vaughan.
 */
public class AugmentedWilsonBalding extends AugmentedTreeOperator {

	public Input<Double> alphaInput = new Input<>("alpha", "Root height proposal parameter", Validate.REQUIRED);
	private double alpha;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		alpha = alphaInput.get();
	}

	/**
	 * Perform proposal.
	 *
	 * @return the log HR
	 */
	@Override
	public double proposal() {
		if (tree.getLeafNodeCount() < 3) throw new IllegalStateException("Tree too small for AugmentedWilsonBalding operator.");
		if (tree.getLeafNodeCount() - tree.getDirectAncestorNodeCount() < 3) return Double.NEGATIVE_INFINITY;

		// Select source node:
		Node srcNode;
		do {
			srcNode = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
		} while (invalidSrcNode(srcNode));
		Node srcNodeP = srcNode.getParent();
		Node srcNodeS = getOtherChild(srcNodeP, srcNode);

		double t_srcNode = srcNode.getHeight();
		double t_srcNodeP = srcNodeP.getHeight();
		double t_srcNodeS = srcNodeS.getHeight();

		// Select destination branch node:
		Node destNode;
		do {
			destNode = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
		} while (invalidDestNode(srcNode, destNode));
		Node destNodeP = destNode.getParent();
		double t_destNode = destNode.getHeight();

		double logHR = 0.0;

		// Handle special cases involving root:

		if (destNode.isRoot()) {
			// FORWARD ROOT MOVE

			// Current augmented trees and eventual direct descendants HR:
			logHR -= augTreeLogHR(srcNode) + augTreeLogHR(srcNodeP) + augTreeLogHR(srcNodeS);

			// Record srcNode grandmother height:
			double t_srcNodeG = srcNodeP.getParent().getHeight();

			// Choose new root height:
			double newTime = t_destNode + Randomizer.nextExponential(1.0 / (alpha * t_destNode));

			// Implement tree changes:
			disconnectBranch(srcNode);
			connectBranchToRoot(srcNode, destNode, newTime);
			tree.setRoot(srcNodeP);
			augTree.updateRates();

			// Resimulate augmented trees:
			try {
				augTree.simulateNodeandDescendants(srcNode);
				augTree.simulateNodeandDescendants(destNode);
				augTree.simulateNodeandDescendants(srcNodeS);
			} catch (AugmentedSimulationException e) {
				return Double.NEGATIVE_INFINITY;
			}

			// New augmented trees HR:
			logHR += augTreeLogHR(srcNodeS) + augTreeLogHR(destNode) + augTreeLogHR(srcNode);

			// HR contribution of topology and node height changes:
			logHR += Math.log(alpha * t_destNode) + (1.0 / alpha) * (newTime / t_destNode - 1.0)
					- Math.log(t_srcNodeG - Math.max(t_srcNode, t_srcNodeS));
			return logHR;
		}

		if (srcNodeP.isRoot()) {
			// BACKWARD ROOT MOVE

			// Current augmented trees HR:
			logHR -= augTreeLogHR(srcNode) + augTreeLogHR(destNode) + augTreeLogHR(srcNodeS);

			// Record old srcNode parent height
			double oldTime = t_srcNodeP;

			// Choose height of new attachement point:
			double min_newTime = Math.max(t_srcNode, t_destNode);
			double t_destNodeP = destNodeP.getHeight();
			double span = t_destNodeP - min_newTime;
			double newTime = min_newTime + span * Randomizer.nextDouble();

			// Implement tree changes:
			disconnectBranch(srcNode);
			connectBranch(srcNode, destNode, newTime);
			tree.setRoot(srcNodeS);
			augTree.updateRates();

			// Resimulate augmented trees:
			try {
				augTree.simulateNodeandDescendants(srcNodeP);
				augTree.simulateNodeandDescendants(srcNodeS);
				augTree.simulateNodeandDescendants(srcNode);
				augTree.simulateNodeandDescendants(destNode);
			} catch (AugmentedSimulationException e) {
				return Double.NEGATIVE_INFINITY;
			}

			// New augmented trees HR:
			logHR += augTreeLogHR(srcNode) + augTreeLogHR(destNode) + augTreeLogHR(srcNodeP);

			// HR contribution of topology and node height changes:
			logHR += Math.log(t_destNodeP - Math.max(t_srcNode, t_destNode)) - Math.log(alpha * t_srcNodeS)
					- (1.0 / alpha) * (oldTime / t_srcNodeS - 1.0);

			return logHR;
		}

		// NON-ROOT MOVE

		// Current augmented trees HR:
		logHR -= augTreeLogHR(srcNode) + augTreeLogHR(destNode) + augTreeLogHR(srcNodeS) + augTreeLogHR(srcNodeP);

		// Record srcNode grandmother height:
		double t_srcNodeG = srcNodeP.getParent().getHeight();

		// Choose height of new attachment point:
		double min_newTime = Math.max(t_destNode, t_srcNode);
		double t_destNodeP = destNodeP.getHeight();
		double span = t_destNodeP - min_newTime;
		double newTime = min_newTime + span * Randomizer.nextDouble();

		// Implement tree changes:
		disconnectBranch(srcNode);
		connectBranch(srcNode, destNode, newTime);

		// Resimulate augmented trees:
		try {
			augTree.simulateNodeandDescendants(srcNodeP);
			augTree.simulateNodeandDescendants(srcNode);
			augTree.simulateNodeandDescendants(destNode);
			augTree.simulateNodeandDescendants(srcNodeS);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}

		// New augmented trees HR:
		logHR += augTreeLogHR(srcNode) + augTreeLogHR(destNode) + augTreeLogHR(srcNodeP) + augTreeLogHR(srcNodeS);

		// HR contribution of topology and node height changes:
		logHR += Math.log(t_destNodeP - Math.max(t_srcNode, t_destNode)) - Math.log(t_srcNodeG - Math.max(t_srcNode, t_srcNodeS));

		return logHR;
	}

	/**
	 * Returns true if srcNode CANNOT be used for the CWBR move.
	 *
	 * @param srcNode the src node
	 * @return true if srcNode is invalid.
	 */
	private boolean invalidSrcNode(Node srcNode) {

		if (srcNode.isRoot() || srcNode.getParent().isFake()) return true;
		Node parent = srcNode.getParent();

		// This check is important for avoiding situations where it is
		// impossible to choose a valid destNode:
		if (parent.isRoot()) {
			Node sister = getOtherChild(parent, srcNode);
			if (sister.isLeaf() || (srcNode.getHeight() >= sister.getHeight())) return true;
		}

		return false;
	}

	/**
	 * Returns true if destNode CANNOT be used for the CWBR move in conjunction with srcNode.
	 *
	 * @param srcNode the src node
	 * @param destNode the dest node
	 * @return true if destNode is invalid.
	 */
	private static boolean invalidDestNode(Node srcNode, Node destNode) {
		if (destNode == srcNode || destNode == srcNode.getParent() || destNode.getParent() == srcNode.getParent()
				|| destNode.isDirectAncestor()) return true;
		
		Node destNodeP = destNode.getParent();
		if (destNodeP != null && (destNodeP.getHeight() <= srcNode.getHeight())) return true;
		return false;
	}

	/**
	 * Disconnect edge <node,node.getParent()> from tree by joining node's sister directly
	 * to node's grandmother (if it exists)
	 *
	 * @param node the node
	 */
	public void disconnectBranch(Node node) {

		// Check argument validity:
		if (node.isRoot()) throw new IllegalArgumentException("Illegal argument to " + "disconnectBranch().");

		Node parent = node.getParent();
		Node sister = getOtherChild(parent, node);

		// Implement topology change.
		if (!parent.isRoot()) {
			replace(parent.getParent(), parent, sister);
		} else {
			sister.setParent(null);
			parent.removeChild(sister);
		}

		// Ensure BEAST knows to update affected likelihoods:
		parent.makeDirty(Tree.IS_FILTHY);
		sister.makeDirty(Tree.IS_FILTHY);
		node.makeDirty(Tree.IS_FILTHY);
	}

	/**
	 * Creates a new branch between node and a new node at time destTime between destBranchBase and its parent.
	 *
	 * @param node the node
	 * @param destBranchBase the dest branch base
	 * @param destTime the dest time
	 */
	public void connectBranch(Node node, Node destBranchBase, double destTime) {

		// Check argument validity:
		if (node.isRoot() || destBranchBase.isRoot()) throw new IllegalArgumentException("Illegal argument to " + "connectBranch().");

		// Obtain existing parent of node and set new time:
		Node parent = node.getParent();
		parent.setHeight(destTime);

		// Implement topology changes:
		replace(destBranchBase.getParent(), destBranchBase, parent);
		destBranchBase.setParent(parent);

		if (parent.getLeft() == node) parent.setRight(destBranchBase);
		else if (parent.getRight() == node) parent.setLeft(destBranchBase);

		// Ensure BEAST knows to update affected likelihoods:
		node.makeDirty(Tree.IS_FILTHY);
		parent.makeDirty(Tree.IS_FILTHY);
		destBranchBase.makeDirty(Tree.IS_FILTHY);
	}

	/**
	 * Set up node's parent as the new root with a height of destTime, with oldRoot as node's new sister.
	 *
	 * @param node the node
	 * @param oldRoot the old root
	 * @param destTime the dest time
	 */
	public void connectBranchToRoot(Node node, Node oldRoot, double destTime) {

		// Check argument validity:
		if (node.isRoot() || !oldRoot.isRoot()) throw new IllegalArgumentException("Illegal argument " + "to connectBranchToRoot().");

		// Obtain existing parent of node and set new time:
		Node newRoot = node.getParent();
		newRoot.setHeight(destTime);

		// Implement topology changes:

		newRoot.setParent(null);

		if (newRoot.getLeft() == node) newRoot.setRight(oldRoot);
		else if (newRoot.getRight() == node) newRoot.setLeft(oldRoot);

		oldRoot.setParent(newRoot);

		// Ensure BEAST knows to recalculate affected likelihood:
		newRoot.makeDirty(Tree.IS_FILTHY);
		oldRoot.makeDirty(Tree.IS_FILTHY);
		node.makeDirty(Tree.IS_FILTHY);
	}
}
