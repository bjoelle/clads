/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.core.Description;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.inference.Operator;
import clads.evolution.tree.AugmentedNode;
import clads.evolution.tree.AugmentedTree;

/**
 * Base class for tree modification operators on augmented trees.
 */
@Description("This operator generates proposals for tree modification on augmented trees.")
public abstract class AugmentedTreeOperator extends Operator {

	public Input<AugmentedTree> augTreeInput = new Input<>("augmentedTree", "Augmented tree on which to operate.", Validate.REQUIRED);

	protected AugmentedTree augTree;
	protected Tree tree;

	@Override
	public void initAndValidate() {
		augTree = augTreeInput.get();
		tree = augTree.getTree();
	}

	/**
	 * Get the log HR for an augmented subtree and potentially of all the direct descendants of the node.
	 *
	 * @param n the node corresponding to the subtree
	 * @return the log HR
	 */
	protected double augTreeLogHR(Node n) {
		augTree.updateRates();
		AugmentedNode root = augTree.getAugmentedTree(n.getNr());
		double rootL = augTree.getSimulatedLambda(n.getParent().getNr());
		double rootM = augTree.getSimulatedMu(n.getParent().getNr());

		double logHR = Math.log(augTree.getAugmentedActiveNodesN(n.getNr()));

		logHR -= subTreeLogLik(root, rootL, rootM, n.getParent().getHeight());
		if (n.isFake()) {
			for (Node ch : n.getChildren()) {
				logHR += augTreeLogHR(ch);
			}
		}
		return logHR;
	}

	/**
	 * Recursive log likelihood of an augmented subtree simulation (minus sampling).
	 *
	 * @param node the root node of the subtree
	 * @param rootL the birth rate above the root
	 * @param rootM the death rate above the root
	 * @param parentHeight the parent height
	 * @return the log likelihood
	 */
	private double subTreeLogLik(AugmentedNode node, double rootL, double rootM, double parentHeight) {
		if (parentHeight == node.getHeight()) {
			return -augTree.getLambdaLogLik(rootL, node.lambda) - augTree.getMuLogLik(rootM, node.mu);
		}
		double logLik = augTree.getLambdaLogLik(rootL, node.lambda) + augTree.getMuLogLik(rootM, node.mu);
		logLik -= (node.lambda + node.mu) * (parentHeight - node.getHeight());

		if (node.isLeaf() && node.getHeight() > 1e-8 && !node.isSampled) {
			logLik += Math.log(node.mu);
		}
		if (!node.isLeaf()) {
			if (!node.isFake()) {
				logLik += Math.log(node.lambda);
			}
			for (Node ch : node.getChildren()) {
				logLik += subTreeLogLik((AugmentedNode) ch, node.lambda, node.mu, node.getHeight());
			}
		}
		return logLik;
	}

	/*
	 * Two methods copied from TreeOperator.
	 */

	/**
	 * Obtain the sister of node "child" having parent "parent".
	 *
	 * @param parent the parent
	 * @param child the child that you want the sister of
	 * @return the other child of the given parent.
	 */
	protected Node getOtherChild(Node parent, Node child) {
		if (parent.getChildCount() != 2) throw new UnsupportedOperationException("Cannot get other child of non-binary node");
		if (parent.getChild(0).getNr() == child.getNr()) {
			return parent.getChild(1);
		}
		return parent.getChild(0);
	}

	/**
	 * Replace node "child" with another node.
	 *
	 * @param node the node
	 * @param child the child
	 * @param replacement the replacement
	 */
	public void replace(Node node, Node child, Node replacement) {
		node.removeChild(child);
		node.addChild(replacement);
		node.makeDirty(Tree.IS_FILTHY);
		replacement.makeDirty(Tree.IS_FILTHY);
	}
}