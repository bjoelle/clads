/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.operators;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;

/**
 * Operator which resimulates an augmented subtree at random.
 */
public class AugmentedTreeSimulationOperator extends AugmentedTreeOperator {

	/**
	 * Perform proposal.
	 *
	 * @return the logHR
	 */
	@Override
	public double proposal() {
		double logHR = 0;
		Node node;
		do {
			node = tree.getNode(Randomizer.nextInt(tree.getNodeCount()));
		} while (node.isRoot() || node.isDirectAncestor()); // TODO change for root edge

		// old tree contribution
		logHR -= augTreeLogHR(node);
		try {
			augTree.simulateNodeandDescendants(node);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}
		// new tree contribution
		logHR += augTreeLogHR(node);
		return logHR;
	}

	/**
	 * If operator is accepted, update the augmented tree. Needed because augmented tree is a state node so doesn't get
	 * flagged as accepted.
	 */
	@Override
	public void accept() {
		augTree.accept();
		super.accept();
	}
}
