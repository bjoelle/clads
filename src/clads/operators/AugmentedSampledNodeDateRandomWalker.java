package clads.operators;

import java.util.ArrayList;
import java.util.List;

import beast.base.core.Input;
import beast.base.evolution.alignment.TaxonSet;
import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedTree.AugmentedSimulationException;
import clads.evolution.tree.SamplingDate;

/**
 * LeaftoSample operator for augmented trees. Allows to switch a direct ancestor to a distinct species and vice versa.
 * Adapted from the SA package, original author Alexandra Gavryushkina.
 */
public class AugmentedSampledNodeDateRandomWalker extends AugmentedTreeOperator {
	final public Input<Double> windowSizeInput = new Input<>("windowSize",
			"the size of the window both up and down when using uniform interval OR standard deviation when using Gaussian",
			Input.Validate.REQUIRED);
	final public Input<TaxonSet> m_taxonsetInput = new Input<>("taxonset",
			"limit scaling to a subset of taxa. By default all tips are scaled.");
	final public Input<Boolean> useGaussianInput = new Input<>("useGaussian",
			"Use Gaussian to move instead of uniform interval. Default false.", false);
	public Input<List<SamplingDate>> samplingDatesInput = new Input<>("samplingDates", "List of sampling dates",
			new ArrayList<SamplingDate>());

	boolean useNodeNumbers;
	List<String> samplingDateTaxonNames = new ArrayList<>();
	int[] taxonIndices;

	double windowSize;
	boolean useGaussian;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		windowSize = windowSizeInput.get();
		useGaussian = useGaussianInput.get();

		for (SamplingDate taxon : samplingDatesInput.get()) {
			samplingDateTaxonNames.add(taxon.taxonInput.get().getID());
		}

		// determine taxon set to choose from
		if (m_taxonsetInput.get() != null) {
			useNodeNumbers = false;
			List<String> sTaxaNames = new ArrayList<String>();
			for (String sTaxon : tree.getTaxaNames()) {
				sTaxaNames.add(sTaxon);
			}

			List<String> set = m_taxonsetInput.get().asStringList();
			int nNrOfTaxa = set.size();
			taxonIndices = new int[nNrOfTaxa];
			int k = 0;
			for (String sTaxon : set) {
				int iTaxon = sTaxaNames.indexOf(sTaxon);
				if (iTaxon < 0) {
					throw new IllegalArgumentException("Cannot find taxon " + sTaxon + " in tree");
				}
				taxonIndices[k++] = iTaxon;
			}
		} else {
			useNodeNumbers = true;
		}
	}

	@Override
	public double proposal() {
		// randomly select a leaf node

		double logHR = 0.0;
		Node node;
		if (useNodeNumbers) {
			int leafNodeCount = tree.getLeafNodeCount();
			int i = Randomizer.nextInt(leafNodeCount);
			node = tree.getNode(i);
		} else {
			int i = Randomizer.nextInt(taxonIndices.length);
			node = tree.getNode(taxonIndices[i]);
		}

		double value = node.getHeight();

		if (value == 0.0) {
			return Double.NEGATIVE_INFINITY;
		}
		double newValue = value;

		boolean drawFromDistribution = samplingDateTaxonNames.contains(node.getID());
		if (drawFromDistribution) {
			SamplingDate taxonSamplingDate = samplingDatesInput.get().get(samplingDateTaxonNames.indexOf(node.getID()));
			double range = taxonSamplingDate.getUpper() - taxonSamplingDate.getLower();
			newValue = taxonSamplingDate.getLower() + Randomizer.nextDouble() * range;
		} else {
			if (useGaussian) {
				newValue += Randomizer.nextGaussian() * windowSize;
			} else {
				newValue += Randomizer.nextDouble() * 2 * windowSize - windowSize;
			}
		}

		Node fake = null;
		double lower, upper;

		if ((node).isDirectAncestor()) {
			fake = node.getParent();
			lower = getOtherChild(fake, node).getHeight();
			if (fake.getParent() != null) {
				upper = fake.getParent().getHeight();
			} else upper = Double.POSITIVE_INFINITY;
		} else {
			// lower = Double.NEGATIVE_INFINITY;
			lower = 0.0;
			upper = node.getParent().getHeight();
		}

		if (newValue < lower || newValue > upper) {
			return Double.NEGATIVE_INFINITY;
		}

		if (newValue == value) {
			// this saves calculating the posterior
			return Double.NEGATIVE_INFINITY;
		}

		logHR -= augTreeLogHR(node);
		if (fake != null) {
			fake.setHeight(newValue);
		}
		node.setHeight(newValue);

		try {
			if (node.isDirectAncestor()) augTree.simulateNodeandDescendants(fake);
			else augTree.simulateNodeandDescendants(node);
		} catch (AugmentedSimulationException e) {
			return Double.NEGATIVE_INFINITY;
		}
		logHR += augTreeLogHR(node);
		return logHR;
	}

}