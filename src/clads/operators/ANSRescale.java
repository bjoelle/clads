package clads.operators;

import beast.base.evolution.tree.Node;
import beast.base.util.Randomizer;
import clads.evolution.tree.AugmentedNode;

public class ANSRescale extends AugmentedNodeShift {

	@Override
	double rootProposal(Node root) {
		// Choose scale factor:
		double u = Randomizer.nextDouble();
		double f = u * rootScaleFactorInput.get() + (1.0 - u) / rootScaleFactorInput.get();
		double logHR = - Math.log(f);

		double lowerBound = Math.max(root.getLeft().getHeight(), root.getRight().getHeight());		
		double oldHeight = root.getHeight();
		double newHeight = lowerBound + f * (root.getHeight() - lowerBound);

		augTree.startEditing(this);
		root.setHeight(newHeight);

		for(Node ch : root.getChildren()) {
			double mult = (newHeight - ch.getHeight()) / (oldHeight - ch.getHeight());
			AugmentedNode chTree = augTree.getAugmentedTree(ch.getNr());

			try {
				int dof = chTree.scaleLengthsWithSampled(mult, ch.getHeight(), Double.NaN);
				logHR += dof * Math.log(mult);
			} catch (IllegalArgumentException e) {
				return Double.NEGATIVE_INFINITY;
			}
		}
		
		return logHR;
	}

	@Override
	double nonRootProposal(Node node) {
		double logHR = 0.0;

		// Select new node height:
		double upperBound = node.getParent().getHeight();
		double lowerBound = Math.max(node.getLeft().getHeight(), node.getRight().getHeight());

		double oldHeight = node.getHeight();
		double newHeight = lowerBound + (upperBound - lowerBound) * Randomizer.nextDouble();

		augTree.startEditing(this);
		node.setHeight(newHeight);

		for(Node ch : node.getChildren()) {
			double mult = (newHeight - ch.getHeight()) / (oldHeight - ch.getHeight());
			AugmentedNode chTree = augTree.getAugmentedTree(ch.getNr());
			try {
				int dof = chTree.scaleLengthsWithSampled(mult, ch.getHeight(), Double.NaN);
				logHR += dof * Math.log(mult);
			} catch (IllegalArgumentException e) {
				return Double.NEGATIVE_INFINITY;
			}
		}

		Node parent = node.getParent();
		double mult = (parent.getHeight() - newHeight) / (parent.getHeight() - oldHeight);
		AugmentedNode nodeTree = augTree.getAugmentedTree(node.getNr());

		try {
			int dof = nodeTree.scaleLengthsWithSampled(mult, parent.getHeight(), newHeight);
			logHR += dof * Math.log(mult);
		} catch (IllegalArgumentException e) {
			return Double.NEGATIVE_INFINITY;
		}
		
		return logHR;
	}

}
