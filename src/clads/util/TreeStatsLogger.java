/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.util;

import java.io.PrintStream;

import beast.base.core.Loggable;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeUtils;

/**
 * Log the colless index and gamma statistic of the sampled multi-rate tree.
 */
public class TreeStatsLogger extends ClaDSLogger implements Loggable {

	private Tree tree;

	@Override
	public void init(PrintStream out) {
		out.print("CollessIndex" + "\t" + "GammaStat" + "\t");
	}

	@Override
	public void log(long sample, PrintStream out) {
		tree = augTree.getTree();
		out.print(calcCollessIndex() + "\t" + calcGammaStatistic() + "\t");
	}

	@Override
	public void close(PrintStream out) {}

	/**
	 * Calculate the colless index.
	 *
	 * @return the colless index
	 */
	double calcCollessIndex() { // adapted from beast-mcmc
		double C = 0;

		int n = tree.getNodeCount();
		for (int i = 0; i < n; i++) {
			Node node = tree.getNode(i);
			if (node.isLeaf()) continue;
			int r = node.getLeft().getLeafNodeCount();
			int s = node.getRight().getLeafNodeCount();
			C += Math.abs(r - s);
		}

		n = tree.getLeafNodeCount();
		return C;
	}

	/**
	 * Calculate the gamma statistic.
	 *
	 * @return the gamma statistic
	 */
	double calcGammaStatistic() { // adapted from beast-mcmc
		int n = tree.getLeafNodeCount();
		double[] g = TreeUtils.getIntervals(tree);
		double T = TreeUtils.getTreeLength(tree, tree.getRoot()); // total branch length

		double sum = 0.0;
		for (int i = 2; i < n; i++) {
			for (int k = 2; k <= i; k++) {
				sum += k * g[k - 2];
			}
		}
		double gamma = ((sum / (n - 2.0)) - (T / 2.0)) / (T * Math.sqrt(1.0 / (12.0 * (n - 2.0))));
		return gamma;
	}

	/**
	 * Calculate the age of second birth event in the tree, or NA if there is no such event.
	 *
	 * @return age as string
	 */
	String calcSecondBirthAge() {
		Double secondBirthAge = null;
		for (Node child : tree.getRoot().getChildren()) {
			if (!child.isLeaf() && (secondBirthAge == null || child.getHeight() > secondBirthAge)) secondBirthAge = child.getHeight();
		}

		return secondBirthAge == null ? "NA" : secondBirthAge.toString();
	}

}
