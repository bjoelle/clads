/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.util;

import beast.base.core.BEASTObject;
import beast.base.core.Input;
import beast.base.core.Input.Validate;
import beast.base.core.Loggable;
import clads.evolution.tree.AugmentedTree;

/**
 * Generic logger class for augmented trees used in ClaDS.
 */
public abstract class ClaDSLogger extends BEASTObject implements Loggable {

	public Input<AugmentedTree> augmentedTreeInput = new Input<>("augmentedTree", "Tree with data augmentation", Validate.REQUIRED);

	AugmentedTree augTree;

	@Override
	public void initAndValidate() {
		augTree = augmentedTreeInput.get();
	}

}
