package clads.util;

import java.io.PrintStream;

import beast.base.core.Loggable;

public class AugmentedTreeLogger extends ClaDSLogger implements Loggable {

	@Override
	public void init(PrintStream out) {
		// NB the taxa count will NOT be correct, but there is no way around it
		// since the number of tips in the augmented tree will change during the run
		augTree.getTree().init(out);
	}
	
	@Override
	public void log(long sample, PrintStream out) {
		out.print("tree STATE_" + sample + " = ");
		out.print(augTree.assembleToNewickFormat());
	}

	@Override
	public void close(PrintStream out) {
		out.print("End;");
	}

}
