/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.util;

import java.io.PrintStream;

import beast.base.core.Loggable;

/**
 * Class for logging the mean birth rate variation mB and (if applicable) the mean death rate variation mD. If m < 1 the
 * corresponding rates decrease from the root, if m > 1 they increase.
 */
public class MeanRateVariationLogger extends ClaDSLogger implements Loggable {

	@Override
	public void init(PrintStream out) {
		String outName;
		if (augTree.getID() == null || augTree.getID().matches("\\s*")) outName = "augmentedTree";
		else outName = augTree.getID();

		out.print(outName + ".mB" + "\t");
		if (!augTree.isTurnoverUsed()) out.print(outName + ".mD" + "\t");
	}

	@Override
	public void log(long sample, PrintStream out) {
		double m = augTree.alphaInput.get().getValue() * Math.exp(Math.pow(augTree.sigmaInput.get().getValue(), 2) / 2.0);
		out.print(m + "\t");
		if (!augTree.isTurnoverUsed()) {
			m = augTree.alphaMInput.get().getValue() * Math.exp(Math.pow(augTree.sigmaMInput.get().getValue(), 2) / 2.0);
			out.print(m + "\t");
		}
	}

	@Override
	public void close(PrintStream out) {}

}
