/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package clads.util;

import java.io.PrintStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;

import beast.base.core.Input;
import beast.base.core.Loggable;
import beast.base.evolution.branchratemodel.BranchRateModel;
import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;

/**
 * Class for logging trees with corresponding birth and death rates for each node.
 */
public class TreeWithRatesLogger extends ClaDSLogger implements Loggable {

	final public Input<BranchRateModel.Base> clockModelInput = new Input<>("branchratemodel", "rate to be logged with branches of the tree");
	final public Input<Boolean> substitutionsInput = new Input<>("substitutions",
			"report branch lengths as substitutions (branch length times clock rate for the branch)", false);

	final public Input<Integer> decimalPlacesInput = new Input<>("dp",
			"the number of decimal places to use writing branch lengths, rates and real-valued metadata, use -1 for full precision (default = full precision)", -1);
	public Input<Boolean> logChangeNodesInput = new Input<>("logChangeNodes", "Whether to log change points on edges as single nodes, default true", true);

	boolean substitutions = false;
	private DecimalFormat df;
	int nodeCount;

	@Override
	public void initAndValidate() {
		super.initAndValidate();

		int dp = decimalPlacesInput.get();
		if (dp < 0) {
			df = null;
		} else {
			// just new DecimalFormat("#.######") (with dp time '#' after the decimal)
			df = new DecimalFormat("#." + new String(new char[dp]).replace('\0', '#'));
			df.setRoundingMode(RoundingMode.HALF_UP);
		}

		// without substitution model, reporting substitutions == reporting branch lengths
		if (clockModelInput.get() != null) {
			substitutions = substitutionsInput.get();
		}
	}

	/**
	 * Create Newick string with metadata for subtree.
	 *
	 * @param node the root of the subtree
	 * @param branchRateModel the branch rate model
	 * @return the Newick string
	 */
	protected String toNewick(Node node, BranchRateModel.Base branchRateModel) {
		StringBuffer buf = new StringBuffer();
		if (!node.isLeaf()) {
			buf.append("(");
			for (int i = 0; i < node.getChildCount(); i++) {
				buf.append(toNewick(node.getChild(i), branchRateModel));
				if (i < node.getChildCount() - 1) buf.append(',');
			}
			buf.append(")");
		} else {
			buf.append(node.getNr() + 1);
		}

		buf.append("[&");

		if (branchRateModel != null) {
			buf.append("rate=");
			appendDouble(buf, branchRateModel.getRateForBranch(node));
			buf.append(",");
		}

		buf.append("lambda=");
		appendDouble(buf, augTree.getSimulatedLambda(node.getNr()));
		buf.append(",mu=");
		appendDouble(buf, augTree.getSimulatedMu(node.getNr()));

		buf.append(']');

		buf.append(":");
		if (substitutions) {
			appendDouble(buf, node.getLength() * branchRateModel.getRateForBranch(node));
		} else {
			appendDouble(buf, node.getLength());
		}
		return buf.toString();
	}

	/**
	 * Append double in given format.
	 *
	 * @param buf the string to append to
	 * @param d the double to append
	 */
	protected void appendDouble(StringBuffer buf, double d) {
		if (df == null) {
			buf.append(d);
		} else {
			buf.append(df.format(d));
		}
	}

	@Override
	public void init(PrintStream out) {
		augTree.getTree().init(out);
	}

	@Override
	public void log(long sample, PrintStream out) {
		Tree tree = augTree.getTree();

		BranchRateModel.Base branchRateModel = clockModelInput.get();
		// write out the log tree with meta data
		out.print("tree STATE_" + sample + " = ");
		out.print(toNewick(tree.getRoot(), branchRateModel));
		out.print(";");
	}

	@Override
	public void close(PrintStream out) {
		augTree.getTree().close(out);
	}
}
