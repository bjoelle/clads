ClaDS
=============

This is a BEAST 2 package which allows for the inference of the ClaDS birth-death prior on phylogenetic trees. 
Birth rates are drawn from the ancestral birth rate at each node following a log-normal distribution. Death rates can either be drawn from a similar log-normal process, or derived from the birth rates using a constant turnover across the tree.
The package integrates a combined ClaDS-FBD model for use on datasets with fossils or sampled ancestors.


NEWS
=============
 * **v2.0.1** Fixed bug with updating the augmented tree when changing the turnover value.
 * **v2.0.2** Fixed BEAUti issues with partitioned alignments.
 * **v2.0.3** Fixed some BEAUti-related bugs.
 * **v2.1.0** Added support for fossil samples & sampled ancestors.
 * **v2.1.1** Edited the default operator setup and tuned operators for performance.
 

 License
-------

This software is free (as in freedom).  With the exception of the
libraries on which it depends, it is made available under the terms of
the GNU General Public Licence version 3, which is contained in this
directory in the file named COPYING.