/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package distributions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.parameter.RealParameter;
import clads.evolution.tree.AugmentedTree;

/**
 * Test augmented tree implementation.
 */

class AugmentedTreeTest {

	/**
	 * Test initialization of augmented tree from Newick.
	 */
	@Test
	void testInitialization_onlyExtant() {
		String newickStr = "(1:2, (2:1, 3:0.5)5:1)4:0;";

		Tree tree = new TreeParser(newickStr, false);

		AugmentedTree augTree = new AugmentedTree();
		assertDoesNotThrow(() -> augTree.initByName("tree", tree, "lambda0", new RealParameter("4"), "alpha", new RealParameter("0.2"),
				"sigma", new RealParameter("0.2"), "turnover", new RealParameter("0.7"), "rho", new RealParameter("0.2")));
	}

	/**
	 * Test initialization of a tree with a fossil tip.
	 */
	void testInitializationFossilTip() {
		String newickStr = "((4:1,5:1)7:10,(((2:2,3:2)9:2)10:1,1:2)8:6)6;";

		Tree tree = new TreeParser(newickStr, true);

		AugmentedTree augTree = new AugmentedTree();

		assertDoesNotThrow(() -> augTree.initByName("tree", tree, "lambda0", new RealParameter("0.5"), "alpha", new RealParameter("0.98"),
				"sigma", new RealParameter("0.2"), "turnover", new RealParameter("0.5")));
	}

	/**
	 * Test initialization of a tree with a sampled ancestor.
	 */
	void testInitializationSA() {
		String newickStr = "((4:1,5:1)7:10,(((2:2,3:2)9:2)10:1,1:0)8:6)6;";
		Tree tree = new TreeParser(newickStr, true);
		AugmentedTree augTree = new AugmentedTree();
		assertDoesNotThrow(() -> augTree.initByName("tree", tree, "lambda0", new RealParameter("0.5"), "alpha", new RealParameter("0.98"),
				"sigma", new RealParameter("0.2"), "turnover", new RealParameter("0.5")));
	}
}
