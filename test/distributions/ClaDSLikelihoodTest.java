/*
 * Copyright (C) 2022 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package distributions;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import beast.base.evolution.tree.Node;
import beast.base.evolution.tree.Tree;
import beast.base.evolution.tree.TreeParser;
import beast.base.inference.OperatorSchedule;
import beast.base.inference.parameter.RealParameter;
import beast.base.util.Randomizer;
import clads.evolution.speciation.ClaDSLikelihood;
import clads.evolution.tree.AugmentedNode;
import clads.evolution.tree.AugmentedTree;
import clads.operators.AugmentedTreeSimulationOperator;

/**
 * Tests for the HR & likelihood calculations.
 */
class ClaDSLikelihoodTest {

	Tree tree;
	AugmentedTree augTree;
	ClaDSLikelihood augLik;

	/**
	 * Set up components for tests.
	 */
	@BeforeEach
	void setUp() {
		String newickStr = "((4:0.150558013,5:0.150558013)7:1.154521107,(((2:0.2610234442,3:0.2610234442)9:0.02397655557)"
				+ "10:0.006852994,1:0.2918529938)8:1.013226126)6;";

		tree = new TreeParser(newickStr, true);

		augTree = new AugmentedTree();
		augTree.initByName("tree", tree, "lambda0", new RealParameter("10.0"), "alpha", new RealParameter("0.98"), "sigma",
				new RealParameter("0.2"), "turnover", new RealParameter("0.5"), "rho", new RealParameter("0.7"));

		augLik = new ClaDSLikelihood();
		augLik.initByName("augmentedTree", augTree, "tree", tree);
	}

	/**
	 * Test HR calculation for augmented tree operator on internal node.
	 */
	@Test
	void testOperatorNode() {
		double logP = augLik.calculateLogP();

		double exp_lnAR = -Math.log(augTree.getSimulatedLambda(8)) - Math.log(augTree.getAugmentedActiveNodesN(8));
		exp_lnAR -= Math.log(1 - 0.7) * findPresentLeaves(augTree.getAugmentedTree(8));
		for (Node ch : tree.getNode(8).getChildren()) {
			exp_lnAR -= augTree.getLambdaLogLik(augTree.getSimulatedLambda(8), augTree.getAugmentedTree(ch.getNr()).lambda);
		}

		AugmentedTreeSimulationOperator op = new AugmentedTreeSimulationOperator();
		op.initByName("augmentedTree", augTree, "weight", 10.0);
		op.setOperatorSchedule(new OperatorSchedule());

		Randomizer.setSeed(12);
		double eff_logHR = op.proposal();
		double newlogP = augLik.calculateLogP();

		exp_lnAR += Math.log(augTree.getSimulatedLambda(8)) + Math.log(augTree.getAugmentedActiveNodesN(8));
		exp_lnAR += Math.log(1 - 0.7) * findPresentLeaves(augTree.getAugmentedTree(8));
		for (Node ch : tree.getNode(8).getChildren()) {
			exp_lnAR += augTree.getLambdaLogLik(augTree.getSimulatedLambda(8), augTree.getAugmentedTree(ch.getNr()).lambda);
		}

		// assert calculation of HR here : logHR + newlogP - logP = log(n') - log(n) + lambda * nu ratio + sampling ratio
		assertEquals(eff_logHR + newlogP - logP, exp_lnAR, 1e-6);
	}

	/**
	 * Test HR calculation for augmented tree operator on leaf.
	 */
	@Test
	void testOperatorLeaf() {
		double logP = augLik.calculateLogP();

		double exp_lnAR = -Math.log(augTree.getAugmentedActiveNodesN(2));
		exp_lnAR -= Math.log(1 - 0.7) * augTree.getAugmentedActiveNodesN(2);

		AugmentedTreeSimulationOperator op = new AugmentedTreeSimulationOperator();
		op.initByName("augmentedTree", augTree, "weight", 10.0);
		op.setOperatorSchedule(new OperatorSchedule());

		Randomizer.setSeed(40);
		double eff_logHR = op.proposal();
		double newlogP = augLik.calculateLogP();

		exp_lnAR += Math.log(augTree.getAugmentedActiveNodesN(2));
		exp_lnAR += Math.log(1 - 0.7) * augTree.getAugmentedActiveNodesN(2);

		// assert calculation of HR here : logHR + newlogP - logP = log(n') - log(n) + lambda * nu ratio + sampling ratio
		assertEquals(eff_logHR + newlogP - logP, exp_lnAR, 1e-6);
	}

	/**
	 * Find number of leaves at present for given augmented tree.
	 *
	 * @param node the root node of the augmented tree
	 * @return the number of leaves
	 */
	private int findPresentLeaves(AugmentedNode node) {
		if (node.isLeaf()) {
			if (node.getHeight() > 1e-8 || node.isSampled) return 0;
			return 1;
		}
		int sum = 0;
		for (Node ch : node.getChildren()) {
			sum += findPresentLeaves((AugmentedNode) ch);
		}
		return sum;
	}

}