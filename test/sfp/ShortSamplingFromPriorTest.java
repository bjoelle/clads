/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfp;

import org.junit.Test;

import test.beast.beast2vs1.trace.LogFileTraces;

/**
 * SamplingFromPrior test with short length.
 */
public class ShortSamplingFromPriorTest extends SamplingFromPriorTest {

	/**
	 * Sfp test for a short chain length.
	 *
	 * @throws Exception if running beast fails
	 */
	@Test
	public void sfpTest() throws Exception {
		XML_FILE = "sampleFromPrior_short.xml";
		tolerances[0] = tolerances[2] = 10;
		tolerances[1] = 20;
		tolerances[3] = 0.5;
		tolerances[4] = 0.8;
		tolerances[5] = 0.7;

		LogFileTraces traces = this.runBeast();
		this.analyzeTraces(traces);
	}
}
