/*
 * Copyright (C) 2016 Joelle Barido-Sottani (joelle.barido-sottani@m4x.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfp;

import org.junit.Test;

import test.beast.beast2vs1.trace.LogFileTraces;

/**
 * SamplingFromPrior test with long length.
 */
public class LongSamplingFromPriorTest extends SamplingFromPriorTest {

	/**
	 * Sfp test for a long chain length.
	 *
	 * @throws Exception if running beast fails
	 */
	@Test
	public void sfpTest() throws Exception {
		XML_FILE = "sampleFromPrior_long.xml";
		tolerances[0] = 6;
		tolerances[1] = 9;
		tolerances[2] = 4;
		tolerances[3] = 0.2;
		tolerances[4] = 0.4;
		tolerances[5] = 0.3;

		LogFileTraces traces = this.runBeast();
		this.analyzeTraces(traces);
	}
}
